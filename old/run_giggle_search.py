#!/usr/bin/python -u

import os, subprocess, warnings, sys, argparse, json
warnings.filterwarnings('ignore')

from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import *


import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
import sv_inferno_functions as sv ## custom python module with SV-INFERNO functions

from importlib import reload    ## remove this later
reload(sdf_fxn)    ## remove this later
reload(sv)    ## remove this later



def gadb_giggle_search_wrapper(sv_type, annot_type, sv_distinct_sdf, giggle_file_sdf, spark_session, flanking=0):
	
	## 1) extract list of GADB files for this annotation type
	#TODO: what format does Hannah / Yi-Fan need? List? sDF?
	gadb_idx_sdf = giggle_file_sdf.filter(giggle_file_sdf.annot_type == annot_type)
	gadb_idx_sdf.show()
	
	## 2) extract query coordinates from sv_sdf
	#TODO: rename cols to chr, start, end?
	query_sdf = sv_distinct_sdf.select(["sv_chr", "sv_start", "sv_end"]).dropDuplicates()
	#TODO?? extract flanking coordinates based on sv_type
	
	
	## 3) run GIGGLE query with selected files:
	#TODO: replace with real function call
	# giggle_results_sdf = gadb.query(sv_sdf, gadb_idx_sdf)

	# return giggle_results_sdf
	return None


# TODO??? this might not be necessary?
def convert_giggle_output(raw_giggle_sdf, query_sdf):
	return None





##########################################################################################
##########################################################################################
# TEMPORARY HACK!!! TODO: replace with real GADB GIGGLE QUERY commands
#----------------------------------------------------------------------------------------#
#<editor-fold desc="TEMPORARY HACK: replace when GIGGLE GADB works!">
def output_file_paths_dict(shell_dir, annot_type, sv_type):
	ws_home = shell_dir.split('/src')[0]
	analysis_results_dir = os.path.join(ws_home, "analysis_results", annot_type + "_overlap")
	analysis_tmp_dir = os.path.join(ws_home, "analysis_tmp")
	
	path_dict = {}
	## output files in 'analysis_results/'
	path_dict['full_annot'] = os.path.join(analysis_results_dir, "tables")
	path_dict['ft_summ_stats'] = os.path.join(analysis_results_dir, "tables")
	path_dict['plots'] = os.path.join(analysis_results_dir, "plots")
	
	## output files in 'analysis_tmp/'
	path_dict['sv_annot_counts'] = os.path.join(analysis_tmp_dir, "annot_counts")
	path_dict['overlap_bed'] = os.path.join(analysis_tmp_dir, "multiway_overlaps", "annot_sv_overlap_bed_files")
	path_dict['overlap_parquet'] = os.path.join(analysis_tmp_dir, "multiway_overlaps", "spark_TCM_parquet")

	#------------------------------------------------------------------------------------#
	##################  TODO REMOVE THESE!!!**
	path_dict['giggle_parsed'] = os.path.join(analysis_tmp_dir, "giggle_parsed", annot_type + "_sv_overlap_parsed_tmp.txt")
	path_dict['full_sv_parquet'] = os.path.join(ws_home, "sv_query", "input_sv_parquet")
	path_dict['distinct_sv_parquet'] = os.path.join(ws_home, "sv_query", "sv_distinct_parquet_" + sv_type)
	#------------------------------------------------------------------------------------#
	
	return path_dict



def get_overlap_sdf_from_text_file(input_overlap_DF_file_path, spark):
	## read in file --> create Spark DataFrame
	overlap_sdf = spark.read.format("csv")\
						.options(header='true', inferschema='true', delimiter='\t')\
						.load(input_overlap_DF_file_path)

	## remove the file extension from the file name (if present)
	overlap_sdf = overlap_sdf.withColumn('ft_file',
										 F.regexp_replace('ft_file', '.bed*', ''))\
							.withColumn('ft_file',
										F.regexp_replace('ft_file', '.txt*', ''))
	return overlap_sdf

##########################################################################################
#</editor-fold>
##########################################################################################



def giggle_search(sv_type, annot_type, sv_distinct_sdf, giggle_file_sdf, path_dict, spark_session, flanking=0, shell_dir='src'):
	#TODO use GADB GIGGLE SEARCH:
	# ## run GIGGLE search
	# giggle_raw_sdf = gadb_giggle_search_wrapper(sv_type, annot_type, sv_distinct_sdf, giggle_file_sdf, spark_session, flanking=0)
	#
	# ## convert giggle output to useable format <--is this necessary???
	# giggle_results_sdf = convert_giggle_output(giggle_raw_sdf, sv_distinct_sdf)
	
	############################################################################
	############# TEMPORARY! remove this!
	
	giggle_results_sdf = get_overlap_sdf_from_text_file(path_dict['giggle_parsed'], spark_session)
	############################################################################
	
	return giggle_results_sdf


if __name__ == "__main__":
	#TODO: add argparse
############################################################################
	## HARDCODED for now: #TODO: add argparse
	sv_type = "DEL"
	annot_type = "homer"
	spark_session_name = 'INFERNO-SV-prototype' ## FUTURE: pass in spark session
	# num_tiss_cat = 32
	
	ws_home = os.getcwd()
	while not os.path.isdir(os.path.join(ws_home, 'src')):
		ws_home = ws_home.rpartition('/')[0]
	sys.path.insert(0, ws_home)
	
	shell_script_dir = os.path.join(ws_home, "src", "spark_output_shell_scripts")
	# py_module_dir = ws_home
	
	giggle_list_file = os.path.join(ws_home, "pipeline_config", "annotation_info", "giggle_files.tsv")
############################################################################
	
	#### Spark Session
	spark = SparkSession.builder.appName(spark_session_name).getOrCreate()
	# sc = spark.sparkContext
	
	
	## generate file path dict
	path_dict = output_file_paths_dict(shell_script_dir, annot_type, sv_type)
	
	#### generate Spark DFs required by giggle_search fxn
	#TODO: pass the Spark DFs in as argparse args
	sv_full_sdf = sdf_fxn.load_sdf_from_parquet(path_dict['full_sv_parquet'], spark).cache()
	# sv_full_sdf.show(3)
	sv_distinct_sdf = sdf_fxn.load_sdf_from_parquet(path_dict['distinct_sv_parquet'], spark).cache()
	# sv_distinct_sdf.show(3)
	giggle_file_sdf = sdf_fxn.load_with_header(giggle_list_file, "\t", spark)
	# giggle_file_sdf.show(3)
	
	
	#### call giggle search -> get overlap sDF
	#TODO: update giggle_search() - remove temp hack
	overlap_sdf = giggle_search(sv_type, annot_type, sv_distinct_sdf, giggle_file_sdf, path_dict, spark, shell_dir=shell_script_dir)
	overlap_sdf.show(3)
	


