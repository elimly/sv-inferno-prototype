#!/usr/bin/python -u

import os, subprocess, warnings, sys, argparse, json
warnings.filterwarnings('ignore')

from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import *


import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
import sv_inferno_functions as sv ## custom python module with SV-INFERNO functions
import run_annot_summ_stats as ss

from importlib import reload    ## remove this later
reload(sdf_fxn)    ## remove this later
reload(sv)    ## remove this later



#------------------------------------------------------------------------------------------#
#**NOTE: this is TEMPORARY!** @TODO: pass in sdf directly

def output_file_paths_dict(shell_dir, annot_type, sv_type):
	ws_home = shell_dir.split('/src')[0]
	analysis_results_dir = os.path.join(ws_home, "analysis_results", annot_type + "_overlap")
	analysis_tmp_dir = os.path.join(ws_home, "analysis_tmp")
	
	path_dict = {}
	## output files in 'analysis_results/'
	path_dict['full_annot'] = os.path.join(analysis_results_dir, "tables")
	path_dict['ft_summ_stats'] = os.path.join(analysis_results_dir, "tables")
	path_dict['plots'] = os.path.join(analysis_results_dir, "plots")
	
	## output files in 'analysis_tmp/'
	path_dict['sv_annot_counts'] = os.path.join(analysis_tmp_dir, "annot_counts")
	path_dict['overlap_bed'] = os.path.join(analysis_tmp_dir, "multiway_overlaps", "annot_sv_overlap_bed_files")
	path_dict['overlap_parquet'] = os.path.join(analysis_tmp_dir, "multiway_overlaps", "spark_TCM_parquet")

	##################  TODO REMOVE THESE!!!**
	path_dict['giggle_parsed'] = os.path.join(analysis_tmp_dir, "giggle_parsed", annot_type + "_sv_overlap_parsed_tmp.txt")
	path_dict['full_sv_parquet'] = os.path.join(ws_home, "sv_query", "input_sv_parquet")
	path_dict['distinct_sv_parquet'] = os.path.join(ws_home, "sv_query", "sv_distinct_parquet_" + sv_type)
	path_dict['subj_sv_stat_total_parquet'] = os.path.join(ws_home, "sv_query", "subj_total_sv_stat_parquet" )
	
	##########################################################################
	
	return path_dict

def get_overlap_sdf_from_text_file(input_overlap_DF_file_path, spark):

	## read in file --> create Spark DataFrame
	overlap_sdf = spark.read.format("csv")\
						.options(header='true', inferschema='true', delimiter='\t')\
						.load(input_overlap_DF_file_path)

	## remove the file extension from the file name (if present)
	overlap_sdf = overlap_sdf.withColumn('ft_file',
										 F.regexp_replace('ft_file', '.bed*', ''))\
							.withColumn('ft_file',
										F.regexp_replace('ft_file', '.txt*', ''))
	
	return overlap_sdf
#------------------------------------------------------------------------------------------#





if __name__ == "__main__":
	print("Started wrapper script. Running in all-spark-notebook Docker container\n")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-a", "--annot_type", action='store')
	parser.add_argument("--run_full", dest="run_full", action='store_true', default=False)
	parser.add_argument("--test_annot", dest="test_annot", action='store_true', default=False)
	parser.add_argument("--test_tiss", dest="test_tiss", action='store_true', default=False)
	parser.add_argument("--write_output", dest="write_output", action='store_true', default=False)
	
	pargs = parser.parse_args()
	annot_type = pargs.annot_type

############################################################################
	## HARDCODED for now:
	sv_type = "DEL"
	spark_session_name = 'INFERNO-SV-prototype' ## FUTURE: pass in spark session
	# num_tiss_cat = 32
	
	shell_script_dir = os.path.join(os.getcwd(), "src", "spark_output_shell_scripts")
	py_module_dir = os.getcwd()
	
	#### Spark Session
	spark = SparkSession.builder.appName(spark_session_name).getOrCreate()
	sc = spark.sparkContext
	
	annot_var_file = os.path.join(os.getcwd(), "pipeline_config", "annotation_info", "annotation_variable_input_table.tsv")
	annot_var_dict = sv.generate_annot_variable_dict(annot_var_file)
############################################################################
	
	print("generating input Spark DFs for run_annot_summ_stats.py fxns")
	
	path_dict = output_file_paths_dict(shell_script_dir, annot_type, sv_type)
	
	## collapsed distinct SV sdf
	sv_distinct_sdf = sdf_fxn.load_sdf_from_parquet(path_dict['distinct_sv_parquet'], spark).cache()
	print("\nSV sDF -- GIGGLE Search input")
	sv_distinct_sdf.show(3)
	
	## overlap sdf
	overlap_sdf = get_overlap_sdf_from_text_file(path_dict['giggle_parsed'], spark)
	print("\nGIGGLE Search results = overlap sDF")
	overlap_sdf.show(3)
	
	## test subsets of fxns
	if not pargs.run_full:
		if pargs.test_tiss:
			ss.test_tissue_specific_fxns(annot_type, annot_var_dict, path_dict, overlap_sdf, sv_distinct_sdf, shell_script_dir, spark, sv_type="DEL")
		elif pargs.test_annot:
			ss.test_annot_summ_stats(annot_type, annot_var_dict, path_dict, overlap_sdf, sv_distinct_sdf, shell_script_dir, spark, sv_type="DEL", spark_context=sc, write_output=pargs.write_output)
	
	## Run full program
	if pargs.run_full:
		## full subject SV sdf
		sv_full_sdf = sdf_fxn.load_sdf_from_parquet(path_dict['full_sv_parquet'], spark).cache()
		sv_full_sdf.show(3)
		## subject level summary stats - SV totals sdf
		subj_sv_total_stat_sdf = sdf_fxn.load_sdf_from_parquet(path_dict['subj_sv_stat_total_parquet'], spark).cache()
		subj_sv_total_stat_sdf.show(3)
	
		## UPDATE: filter sv_full_subj)_sdf & subj_sv_stat_totals_sdf BEFORE passing to run_annot_summ_stats
		sv_full_sdf_sv_type_filtered = sv_full_sdf.filter(sv_full_sdf.sv_type == sv_type)
		# print("# rows in sv_type filtered sv_full_subj_sdf =  \t", sv_full_sdf_sv_type_filtered.count())
		sv_full_sdf_sv_type_filtered.show(3)
	
		subj_sv_total_stat_sdf_sv_type_filtered = subj_sv_total_stat_sdf.filter(subj_sv_total_stat_sdf.sv_type == sv_type)
		# print("# rows in sv_type filtered subj_sv_total_stat_sdf = \t", subj_sv_total_stat_sdf_sv_type_filtered.count())
		subj_sv_total_stat_sdf_sv_type_filtered.show(3)
	
	
		print("calling run_annot_summ_stats.py process_giggle_results()")
		#FIXME: calling fxn in module instead of running subprocess
		ss.process_giggle_results(annot_type, annot_var_dict, path_dict, overlap_sdf, sv_distinct_sdf, sv_full_sdf_sv_type_filtered, subj_sv_total_stat_sdf_sv_type_filtered, shell_script_dir, spark, sv_type="DEL", subj_info=True, write_subj=False, spark_context=sc)

	
	
	print("wrapper script complete")