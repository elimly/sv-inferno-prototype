#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
*** Spark DataFrame Functions ***

NOTES:
	- these functions have not been time tested
	- some of the functions call shell scripts (located in the src directory in this repo)
"""

import subprocess, os
from itertools import chain
from importlib import reload

## PySpark imports
import pyspark.sql.functions as F
from pyspark.sql import Row
from pyspark.sql.types import *

import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
reload(sdf_fxn)


## helper fxn to rename columns for output files
def rename_annotation_column_dict(column_list, rename_ft_dict):
	rename_col_dict = {col:col.replace(key, rename_ft_dict[key])
						for key in rename_ft_dict.keys()
						for col in column_list
						if key in col}
	
	for old_name,new_name in rename_col_dict.items():
		for ft in rename_ft_dict.keys():
			if ft in new_name:
				rename_col_dict[old_name] = new_name.replace(ft, rename_ft_dict[ft])
	
	return rename_col_dict





def generate_sv_annot_counts(ovrlp_sdf, sv_col, ft_col, ovrlp_col):
	ovrlp_sdf = ovrlp_sdf.dropDuplicates()\
					 .orderBy(sv_col)
	
	sv_summ_stat_sdf = ovrlp_sdf.groupBy(sv_col + ['sv_len'])\
					.agg(F.count('*').alias('Total_#_ft'),
						 F.countDistinct('ft_name').alias('#_distinct_ft_names'),
						 F.countDistinct(*ft_col).alias('#_distinct_ft_coord'),
						 F.countDistinct(*ovrlp_col).alias('#_distinct_ovrlp_coord'))\
					.orderBy(sv_col)
	return sv_summ_stat_sdf





#NOTE: assuming duplicates have been removed from overlap sDF!!!
def generate_annot_total_summary(sv_type, annot_type, annot_var_dict, ovrlp_sdf, sv_cols, ft_cols, ovrlp_cols, spark_context):
	## set up variables
	stat_cols = ['#_ft_coord_with_>1_ft_name', '%_ft_coord_with_>1_ft_name',
				  '#_ovrlp_coord_with_>1_ft_name', '%_ovrlp_coord_with_>1_ft_name',
				  '#_ovrlp_coord_with_>1_ft_coord', '%_ovrlp_coord_with_>1_ft_coord',
				  '#_ovrlp_coord_with_>1_sv_coord', '%_ovrlp_coord_with_>1_sv_coord']

	## 1) calculate overall Totals
	total_sdf = ovrlp_sdf.groupBy()\
						.agg(F.countDistinct('ft_name').alias('#_distinct_ft_names'),
							 F.countDistinct(*(['ft_name'] + ft_cols)).alias('Total_#_ft'),
							 F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							 F.countDistinct(*sv_cols).alias('#_distinct_sv_coord'))
	
	total_cols_select = total_sdf.columns + stat_cols

	## 2) TMP sdf for calculating per ft_coord
	ft_coord = ovrlp_sdf.select(ft_cols + ['ft_name'])\
						.dropDuplicates()\
						.groupBy(ft_cols)\
						.agg(F.count('*').alias('#_ft_names_per_ft_coord'))
	
	## 3) TMP sdf for calculating per ovrlp_coord
	ovrlp_coord = ovrlp_sdf.select(ovrlp_cols + sv_cols + ft_cols + ['ft_name'])\
						.dropDuplicates()\
						.groupBy(ovrlp_cols)\
						.agg(F.countDistinct('ft_name').alias('#_ft_names_per_ovrlp_coord'),
							 F.countDistinct(*ft_cols).alias('#_ft_coord_per_ovrlp_coord'),
							 F.countDistinct(*sv_cols).alias('#_sv_coord_per_ovrlp_coord'))
	
	## 4) add 4 counts from TMP sDFs
	total_sdf = total_sdf.withColumn('#_ft_coord_with_>1_ft_name',
							F.lit(ft_coord.filter(ft_coord['#_ft_names_per_ft_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_ft_name',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_ft_names_per_ovrlp_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_ft_coord',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_ft_coord_per_ovrlp_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_sv_coord',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_sv_coord_per_ovrlp_coord'] > 1).count()))
	
	## 5) calculate % for the 4 counts added above
	total_sdf = total_sdf.withColumn('%_ft_coord_with_>1_ft_name',
							F.lit(total_sdf['#_ft_coord_with_>1_ft_name'] / total_sdf['#_distinct_ft_coord']) )\
					.withColumn('%_ovrlp_coord_with_>1_ft_name',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_ft_name'] / total_sdf['#_distinct_ovrlp_coord']))\
					.withColumn('%_ovrlp_coord_with_>1_ft_coord',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_ft_coord'] / total_sdf['#_distinct_ovrlp_coord']))\
					.withColumn('%_ovrlp_coord_with_>1_sv_coord',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_sv_coord'] / total_sdf['#_distinct_ovrlp_coord']))
	
	## 6) reorder & rename columns
	total_sdf = total_sdf.select(total_cols_select)
	rename_dict =  rename_annotation_column_dict(total_cols_select, annot_var_dict[annot_type]['rename_ft_dict'])
	total_sdf = sdf_fxn.rename_spark_columns(total_sdf, rename_dict)
	
	## 7) transpose sDF
	total_sdf = sdf_fxn.transpose_spark_df_row(total_sdf.collect()[0], spark_context, "Overall Totals", cast_type="double")
	
	## rename new columns
	total_sdf = total_sdf.withColumnRenamed('key', annot_var_dict[annot_type]['annot_name'] + " " + sv_type)
	return total_sdf





#NOTE: assuming duplicates have been removed from overlap sDF!!!
def generate_annot_feature_summ_stats(ovrlp_sdf, sv_cols, ft_cols, ovrlp_cols):
	## 1) calc totals per feature name
	annot_count_sdf = ovrlp_sdf.groupBy('ft_name')\
						.agg(F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							 F.countDistinct(*sv_cols).alias('#_distinct_sv_coord'))
	
	## 2) calc totals per (feature name && feature coord)
	ft_count_sdf = ovrlp_sdf.groupBy(['ft_name'] + ft_cols)\
						.agg(F.countDistinct(*sv_cols).alias('#_sv_per_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_ovrlp_coord_per_ft_coord'))\
						.orderBy('ft_name', ascending=False)
	
	## 3) calc averages per annot feature name
	ft_stat_sdf = ft_count_sdf.groupBy('ft_name')\
						.agg(F.avg('#_ovrlp_coord_per_ft_coord').alias('#_ovrlp_coord_per_ft_coord.AVG'),
							 F.avg('#_sv_per_ft_coord').alias('#_sv_per_ft_coord.AVG'))
	
	## 4) count # ft & overlap coord per SV (per feature name)
	ft_count_perSV_sdf = ovrlp_sdf.groupBy(['ft_name'] + sv_cols + ['sv_len'])\
						.agg(F.countDistinct(*ft_cols).alias('#_ft_coord_per_sv_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_ovrlp_coord_per_sv_coord'))\
						.orderBy('ft_name', ascending=False)
	
	## 5) calc averages per SV, per annot feature name
	ft_stat_perSV_sdf = ft_count_perSV_sdf.groupBy('ft_name')\
						.agg(F.avg('#_ft_coord_per_sv_coord').alias('#_ft_coord_per_sv_coord.AVG'),
							 F.avg('#_ovrlp_coord_per_sv_coord').alias('#_ovrlp_coord_per_sv_coord.AVG'),
	                         F.avg('sv_len').alias('sv_len.AVG'),
	                         F.min('sv_len').alias('sv_len.MIN'),
	                         F.max('sv_len').alias('sv_len.MAX'),
	                         F.stddev('sv_len').alias('STDDEV.sv_len'))
	
	## 6) join ft name totals & averages Spark DFs
	annot_summ_stat_sdf = annot_count_sdf.join(ft_stat_sdf, on='ft_name', how='inner')\
						.join(ft_stat_perSV_sdf, on='ft_name', how='inner')\
						.orderBy('ft_name', ascending=False)
	return annot_summ_stat_sdf




def subject_annotation_summ_stats(ovrlp_subj_sdf, sv_full_subj_sdf, sv_annot_count_sdf, subj_sv_stats_sdf, sv_cols, ft_cols, ovrlp_cols):
	
	ovrlp_len = ovrlp_cols[0].strip('chr') + 'len'
	reorder_cols = ['subj_id'] + sv_cols + ['sv_len', 'sv_type'] + sv_annot_count_sdf.columns[4:]
	
	#### 1) subject - annot TOTALs:
	subj_total_sdf = ovrlp_subj_sdf.groupBy('subj_id')\
						.agg(F.count('*').alias('Total_#_ft'),
							F.countDistinct('ft_name').alias('#_distinct_ft_names'),
							F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							F.avg('ft_len').alias("ft_len.AVG"),
							F.min('ft_len').alias("ft_len.MIN"),
							F.max('ft_len').alias("ft_len.MAX"),
							F.stddev('ft_len').alias("ft_len.STDDEV"),
							F.avg(ovrlp_len).alias("overlap_len.AVG"),
							F.min(ovrlp_len).alias("overlap_len.MIN"),
							F.max(ovrlp_len).alias("overlap_len.MAX"),
							F.stddev(ovrlp_len).alias("overlap_len.STDDEV"),
							F.avg('proportion_of_ft').alias("proportion_of_ft.AVG"),
							F.min('proportion_of_ft').alias("proportion_of_ft.MIN"),
							F.max('proportion_of_ft').alias("proportion_of_ft.MAX"),
							F.stddev('proportion_of_ft').alias("proportion_of_ft.STDDEV"),
							F.avg('proportion_of_sv').alias("proportion_of_sv.AVG"),
							F.min('proportion_of_sv').alias("proportion_of_sv.MIN"),
							F.max('proportion_of_sv').alias("proportion_of_sv.MAX"),
							F.stddev('proportion_of_sv').alias("proportion_of_sv.STDDEV"))\
						.cache()


	#### 2) subject - annot per SV:
	## join full_subj_sv_sdf & sv_annot_count_sdf --> add subj IDs to each SV
	subj_sv_annot_count_sdf = sv_full_subj_sdf.join(sv_annot_count_sdf, on=sv_cols + ['sv_len'], how="inner")\
											.select(reorder_cols)\
											.repartition('subj_id') ## remove repartition to speed up??

	subj_sv_sdf = subj_sv_annot_count_sdf.groupBy(['subj_id'])\
						.agg(F.avg('Total_#_ft').alias('perSV.Total_#_ft.AVG'),
							 F.min('Total_#_ft').alias('perSV.Total_#_ft.MIN'),
							 F.max('Total_#_ft').alias('perSV.Total_#_ft.MAX'),
							 F.stddev('Total_#_ft').alias('perSV.Total_#_ft.STDDEV'),
							 F.avg('#_distinct_ft_names').alias('perSV.#_distinct_ft.AVG'),
							 F.min('#_distinct_ft_names').alias('perSV.#_distinct_ft.MIN'),
							 F.max('#_distinct_ft_names').alias('perSV.#_distinct_ft.MAX'),
							 F.stddev('#_distinct_ft_names').alias('perSV.#_distinct_ft.STDDEV'),
							 F.avg('#_distinct_ft_coord').alias('perSV.#_ft_coord.AVG'),
							 F.min('#_distinct_ft_coord').alias('perSV.#_ft_coord.MIN'),
							 F.max('#_distinct_ft_coord').alias('perSV.#_ft_coord.MAX'),
							 F.stddev('#_distinct_ft_coord').alias('perSV.#_ft_coord.STDDEV'),
							 F.avg('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.AVG'),
							 F.min('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.MIN'),
							 F.max('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.MAX'),
							 F.stddev('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.STDDEV'))\
						.cache()

	#### 3) join with Subject SV total sDF:
	subj_stat_sdf = subj_sv_stats_sdf.join(subj_total_sdf, on='subj_id', how="inner")\
									.join(subj_sv_sdf, on='subj_id', how="inner")\
									.orderBy('subj_id')\
									.cache()

	return subj_stat_sdf



def subject_sv_totals(sv_subj_sdf):
	## drop duplicates & repartition by subject ID
	tmp_subj_sv_sdf = sv_subj_sdf.dropDuplicates().repartition("subj_id").cache()

	## 1) count by SV type
	subj_sdf_sv_type = tmp_subj_sv_sdf.groupBy(['subj_id', 'sv_type'])\
								.agg(F.count('*').alias("num_sv"),
									 F.sum("sv_len").alias("Total_sv_len"),
									 F.avg("sv_len").alias("sv_len.AVG"),
									 F.min("sv_len").alias("sv_len.MIN"),
									 F.max("sv_len").alias("sv_len.MAX"),
									 F.stddev("sv_len").alias("sv_len.STDDEV"))
	select_cols = ["`"+c+"`" for c in subj_sdf_sv_type.columns]
	
	## 2) totals for ALL SVs combined
	subj_sdf_all_sv = tmp_subj_sv_sdf.groupBy('subj_id')\
								.agg(F.count('*').alias("num_sv"),
									 F.sum("sv_len").alias("Total_sv_len"),
									 F.avg("sv_len").alias("sv_len.AVG"),
									 F.min("sv_len").alias("sv_len.MIN"),
									 F.max("sv_len").alias("sv_len.MAX"),
									 F.stddev("sv_len").alias("sv_len.STDDEV"))\
								.withColumn("sv_type", F.lit("ALL"))\
								.select(select_cols)

	## 3) concat DFs
	subj_sdf = subj_sdf_sv_type.union(subj_sdf_all_sv)\
							.orderBy(["subj_id", "sv_type"], ascending=[1, 1])\
							.fillna(0)\
							.cache()

	tmp_subj_sv_sdf.unpersist()
	return subj_sdf


