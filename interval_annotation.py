import os,  warnings, ast
warnings.filterwarnings('ignore')
import pandas as pd

# from importlib import reload
from itertools import chain

from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F

import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
# reload(sdf_fxn)    ## remove this later





####################################################################
# **TODO: decide what to do with HOMER overlap duplicate rows!  **
####################################################################
# >the HOMER bed file contains 4662 TFs that "duplicated" without using column 5 [1 indexed; col5 = score??]
#  - these TFs have identical coord & names & strand, but vary by 1 in column 5 [1 indexed]
# **DECIDE: include col5 or drop duplicates.**
#------------------------------------------------------------------#




##################################################################################################
#### Helper fxns
##################################################################################################

def rename_annotation_column_dict(column_list, rename_ft_dict):
	print("running rename_annotation_column_dict fxn")
	
	rename_col_dict = {col:col.replace(key, rename_ft_dict[key])
						for key in rename_ft_dict.keys()
						for col in column_list
						if key in col}
	
	for old_name,new_name in rename_col_dict.items():
		for ft in rename_ft_dict.keys():
			if ft in new_name:
				rename_col_dict[old_name] = new_name.replace(ft, rename_ft_dict[ft])
	
	return rename_col_dict


def rename_annotation_columns(sdf, rename_ft_dict):
	print("running rename_annotation_columns fxn")
	
	## generate new column names from sdf
	rename_col_dict = rename_annotation_column_dict(sdf.columns, rename_ft_dict)
	
	## rename columns using spark_df_functions
	renamed_sdf = sdf_fxn.rename_spark_columns(sdf, rename_col_dict)
	
	return renamed_sdf
	


def get_start_end_cols(query_cols, ft_cols, ovrlp_cols):
	cols_start_end = [query_cols[0].split('_')[0]+'_start', query_cols[0].split('_')[0]+'_end',
					  ft_cols[0].split('_')[0]+'_start', ft_cols[0].split('_')[0]+'_end',
					  ovrlp_cols[0].split('_')[0]+'_start', ovrlp_cols[0].split('_')[0]+'_end']
	return cols_start_end






##################################################################################################
## Step 0: config & setup
##################################################################################################

def generate_annot_config_dict(annot_table_fp):
	print("running generate_annot_config_dict fxn")
	
	## read in table -> Pandas DF
	annot_df = pd.read_table(annot_table_fp, index_col='annot_type')

	## convert DF --> dict
	variable_dict = annot_df.where((pd.notnull(annot_df)), None).to_dict(orient='index')
	# print(variable_dict)

	## convert dict string to dictionary
	for annot in variable_dict:
		for v in variable_dict[annot]:
			if type(variable_dict[annot][v]) is str:
				if ('{' in variable_dict[annot][v]) | ('[' in variable_dict[annot][v]) :
					variable_dict[annot][v] = ast.literal_eval(variable_dict[annot][v])
	return variable_dict





##################################################################################################
## Step 1: run GIGGLE search
##################################################################################################

def run_GIGGLE_search(query_sdf, annot_type, gadb_giggle_str, spark_session):
	print("\n\nrunning \'GIGGLE search\' demo -- annotation = ", annot_type)
	#TODO: implement!
	# gig_result_sdf = gig.search(query_sdf)
	
	#TEMP - remove!!
	gig_pd_df = pd.read_table(gadb_giggle_str, sep='\t', header=0)
	gig_result_sdf = spark_session.createDataFrame(gig_pd_df)
	
	return gig_result_sdf





##################################################################################################
## Step 2: process GIGGLE results sDF & extract overlap information
##################################################################################################

#### 2.a) extract overlap data:
def extract_overlap_coord(ovrlp_sdf, query_cols, ft_cols, ovrlp_cols):
	print("running extract_overlap_coord fxn")

	#TODO: decide what to do with HOMER duplicates!!!
	## drop duplicate from ovrlp_sdf (if necessary)
	ovrlp_sdf = ovrlp_sdf.dropDuplicates()

	## extract overlap info:
	if 'chr' in ovrlp_cols[0]:
		ovrlp_start = ovrlp_cols[1]
		ovrlp_end = ovrlp_cols[2]
		ovrlp_sdf = ovrlp_sdf.withColumn(ovrlp_cols[0], F.col(query_cols[0]))
		
	else:
		ovrlp_start = ovrlp_cols[0]
		ovrlp_end = ovrlp_cols[1]
	
	ovrlp_sdf = ovrlp_sdf.withColumn(ovrlp_start, F.greatest(F.col(query_cols[1]), F.col(ft_cols[1])))\
						.withColumn(ovrlp_end, F.least(F.col(query_cols[2]), F.col(ft_cols[2])))
	
	return ovrlp_sdf


#### 2.b) extract overlap length & proportions:
def extract_overlap_length_data(ovrlp_sdf, q_start, q_end, ft_start, ft_end, ovrlp_start, ovrlp_end):
	print("running extract_overlap_length_data fxn")
	
	q_len = q_start.split('_')[0]+"_len"
	ft_len = ft_start.split('_')[0]+"_len"
	ovrlp_len = ovrlp_start.split('_')[0]+"_len"
	
	ovrlp_sdf = ovrlp_sdf.withColumn(q_len, ovrlp_sdf[q_end]+1 - ovrlp_sdf[q_start])\
						.withColumn(ft_len, ovrlp_sdf[ft_end]+1 - ovrlp_sdf[ft_start])\
						.withColumn(ovrlp_len, ovrlp_sdf[ovrlp_end]+1 - ovrlp_sdf[ovrlp_start])
	
	ovrlp_sdf = ovrlp_sdf.withColumn('proportion_of_query', ovrlp_sdf[ovrlp_len] / ovrlp_sdf[q_len])\
						.withColumn('proportion_of_ft', ovrlp_sdf[ovrlp_len] / ovrlp_sdf[ft_len])
	return ovrlp_sdf




##################################################################################################
## Step 3: add annotation specific info - from file or DF
##################################################################################################

#### 3) join overlap sDF with annotation specific file
## helper fxns
def add_annot_info_from_df(ovrlp_sdf, annot_info_sdf, join_col_ovrlp, join_col_annot):
	print("running add_annot_info_from_df fxn")
	
	## expression for join operation
	if join_col_ovrlp == join_col_annot:
		on_expr = join_col_ovrlp
	else:
		on_expr = (F.col(join_col_ovrlp)==F.col(join_col_annot))
	
	## join overlap & annot info DFs
	join_sdf = ovrlp_sdf.join(annot_info_sdf, on=on_expr, how='left')
	
	## reorder output columns
	reorder_cols = ovrlp_sdf.columns + annot_info_sdf.columns
	reorder_cols.remove(join_col_ovrlp)
	reorder_cols.remove(join_col_annot)
	
	return join_sdf.select(reorder_cols + [join_col_ovrlp])



def load_annot_info_file(annot_info_file, sep, spark_session):
	print("running load_annot_info_file fxn")
	
	if not os.path.isfile(annot_info_file): ## temporary
		print("ERROR: the specified tissue info file path is NOT valid")
		return None ## change this?
	
	## load annot info file
	annot_info_sdf = sdf_fxn.load_with_header(annot_info_file, sep, spark_session)
	return annot_info_sdf



def add_annot_info_from_file(ovrlp_sdf, annot_info_file, join_col_ovrlp, join_col_annot, spark_session, sep=','):
	print("running add_annot_info_from_file fxn")
	
	## load tissue info file
	annot_info_sdf = load_annot_info_file(annot_info_file, sep, spark_session)
	
	## join overlap sDF & tissue info sDF
	ovrlp_annot_info_sdf = add_annot_info_from_df(ovrlp_sdf, annot_info_sdf, join_col_ovrlp, join_col_annot)
	
	return ovrlp_annot_info_sdf
	


#### 3.b) add tissue information from file
def add_tissue_info_from_file(ovrlp_sdf, tiss_info_file, tiss_info_cols, join_col_ovrlp, join_col_tiss, spark_sess, sep=','):
	## error handling
	if join_col_tiss not in tiss_info_cols:
		tiss_info_cols = [join_col_tiss] + tiss_info_cols
	
	if not os.path.isfile(tiss_info_file): ## temporary
		print("ERROR: the specified tissue info file path is NOT valid")
		return ovrlp_sdf ## change this?
	
	## load tissue info file
	tiss_info_sdf = sdf_fxn.load_with_header(tiss_info_file, sep, spark_sess)
	
	## join overlap sDF with tissue info sDF
	join_sdf = add_annot_info_from_df(ovrlp_sdf, tiss_info_sdf.select(tiss_info_cols), join_col_ovrlp, join_col_tiss)
	
	return join_sdf


##################################################################################################
## Step 4: interval summary
##################################################################################################

def interval_summary(ovrlp_sdf, annot_type, annot_abr, query_cols, ft_cols, ovrlp_cols):
	print("running interval_summary fxn")
	
	ft_name_col = ft_cols[0].split('_')[0] + '_name'
	
	## drop duplicates ** NOTE: some may still exist if hit string / 5th col in bed6 is included
	ovrlp_grp =  ovrlp_sdf.dropDuplicates().groupBy(query_cols)
	
	
	#TODO: decide what to do with HOMER duplicates!!! if count('*') -> homer DF will contain duplicates!!
	summary_sdf = ovrlp_grp.agg(F.count('*').alias('num_'+annot_abr+'_ovrlps'),
								  F.countDistinct(*ovrlp_cols).alias('num_'+annot_abr+'_ovrlp_coord'),
								  F.countDistinct(*ft_cols).alias('num_'+annot_abr+'_ft_coord'),
								  F.countDistinct(ft_name_col).alias('num_'+annot_abr+'_ft_names'),
								  F.collect_set(ft_name_col).alias(annot_abr+'_ft_names'))
	# summary_sdf.show(3)
	return summary_sdf



def interval_summary_tissue(ovrlp_sdf, annot_type, annot_abr, query_cols, ft_cols, ovrlp_cols):
	print("running interval_summary_tissue fxn")
	
	ovrlp_grp =  ovrlp_sdf.dropDuplicates().groupBy(query_cols)

	summary_sdf = ovrlp_grp.agg(F.count('*').alias('num_'+annot_abr+'_ovrlps'),
								  F.countDistinct(*ovrlp_cols).alias('num_'+annot_abr+'_ovrlp_coord'),
								  F.countDistinct(*ft_cols).alias('num_'+annot_abr+'_ft_coord'),
								  F.countDistinct('tissue_name').alias('num_'+annot_abr+'_tissues'),
								  F.collect_set('tissue_name').alias(annot_abr+'_tissues'),
								  F.countDistinct('tiss_cat_name').alias('num_'+annot_abr+'_classes'),
								  F.collect_set('tiss_cat_name').alias(annot_abr+'_classes'))
	# summary_sdf.show(3)

	if ('roadmap' in annot_type.lower()) | ('chromHMM' in annot_type.lower()):
		ft_name_col = ft_cols[0].split('_')[0] + '_name'
		hmm_state_tmp_sdf = ovrlp_grp.agg(F.countDistinct(ft_name_col).alias('num_hmm_enh_states'),
										  F.collect_set(ft_name_col).alias('hmm_enh_states'))

		summary_sdf = summary_sdf.join(hmm_state_tmp_sdf, on=query_cols, how='outer')

	return summary_sdf




##################################################################################################
## Step 5: tissue specific
##################################################################################################

def get_tissue_cat_total_counts(tiss_info_sdf, annot_type, annot_abr, ft_cols, ovrlp_cols, num_tissue_cat, spark_session):
	print("running get_tissue_cat_total_counts fxn")
	
	## create tmp sDF with all Tissue Classes & specify the Annotation & Feature type:
	tmp_pd_df = pd.DataFrame([x for x in range(1, num_tissue_cat+1)], columns=['tiss_cat_id'])
	tmp_tclass_sdf = spark_session.createDataFrame(tmp_pd_df)
	# tmp_tclass_sdf.show(3)
	
	## get Tissue Class totals:
	tiss_cat_totals_sdf = tiss_info_sdf.dropDuplicates()\
									.groupBy('tiss_cat_id')\
									.agg(F.count('*').alias('num_' + annot_abr + '_ovrlp'),
										 F.countDistinct('tissue_name').alias('num_' + annot_abr + '_tissue_w_ovrlp'),
										 F.countDistinct(*ovrlp_cols).alias('num_' + annot_abr + '_ovrlp_coord'),
										 F.countDistinct(*ft_cols).alias('num_' + annot_abr + '_ft_coord'))
	# tiss_cat_totals_sdf.show(3)

	## join Tissue Class totals with full list of Tissue Classes (in case 1+ TC doesn't have any overlaps in a data set)
	tc_totals_sdf = tiss_cat_totals_sdf.join(tmp_tclass_sdf, on='tiss_cat_id', how='outer')\
								.na.fill(0)\
								.orderBy('tiss_cat_id')

	return tc_totals_sdf


def generate_tiss_category_matrix(tiss_info_sdf, query_cols, grpby_cols, col_tissue, col_category, col_cat_id, num_tissue_classes):
	print("running generate_tiss_category_matrix fxn")
	
	tc_cols_rename_dict = dict(zip([str(x) for x in range(1, num_tissue_classes+1)],
								   ['tc'+str(y) for y in range(1, num_tissue_classes+1)]))

	## create groupBy object:
	grp = tiss_info_sdf.select(grpby_cols + [col_tissue, col_category, col_cat_id])\
									.dropDuplicates()\
									.orderBy(grpby_cols + [col_tissue])\
									.groupBy(grpby_cols)

	## collect list of tissues & tissue categories
	tiss_list_sdf = grp.agg(F.collect_set(col_tissue).alias('tissues'))
	# tiss_list_sdf.show(3)
	
	tiss_cat_list_sdf = grp.agg(F.collect_set(col_category).alias('tiss_classes'))
	# tiss_cat_list_sdf.show(3)

	## create Tissue Class Matrix columns (values = # of distinct Tissues per TC)
	tc_num_tiss_sdf = grp.pivot(col_cat_id, [x for x in range(1, num_tissue_classes+1)])\
							.agg(F.collect_list(col_tissue))
	## rename columns
	tc_num_tiss_sdf = sdf_fxn.rename_spark_columns(tc_num_tiss_sdf, tc_cols_rename_dict)
	
	new_TCM_sdf = tiss_list_sdf.join(tiss_cat_list_sdf, on=grpby_cols, how='outer')\
							.join(tc_num_tiss_sdf, on=grpby_cols, how='outer')\
							.orderBy(query_cols)
	
	return new_TCM_sdf


def generate_tiss_category_count_matrix(tiss_info_sdf, query_cols, grpby_cols, col_tissue, col_category, col_cat_id, num_tissue_classes):
	print("running generate_tiss_category_count_matrix fxn")
	
	tc_cols_rename_dict = dict(zip([str(x) for x in range(1, num_tissue_classes+1)],
								   ['tc'+str(y) for y in range(1, num_tissue_classes+1)]))

	## create groupBy object:
	grp = tiss_info_sdf.select(grpby_cols + [col_tissue, col_category, col_cat_id])\
									.dropDuplicates()\
									.orderBy(grpby_cols + [col_tissue])\
									.groupBy(grpby_cols)

	## collect list of tissues & tissue categories
	tiss_list_sdf = grp.agg(F.collect_set(col_tissue).alias('tissues'))
	# tiss_list_sdf.show(3)
	
	tiss_cat_list_sdf = grp.agg(F.collect_set(col_category).alias('tiss_classes'))
	# tiss_cat_list_sdf.show(3)

	## create Tissue Class Matrix columns (values = # of distinct Tissues per TC)
	tc_num_tiss_sdf = grp.pivot(col_cat_id, [x for x in range(1, num_tissue_classes+1)])\
							.agg(F.count(col_tissue))
	## rename columns
	tc_num_tiss_sdf = sdf_fxn.rename_spark_columns(tc_num_tiss_sdf, tc_cols_rename_dict)
	
	new_TCM_sdf = tiss_list_sdf.join(tiss_cat_list_sdf, on=grpby_cols, how='outer')\
							.join(tc_num_tiss_sdf, on=grpby_cols, how='outer')\
							.orderBy(query_cols)
	
	return new_TCM_sdf




## Function to get annotation Counts (Total # of features, # distinct tissues & # of overlaps) per TClass per SV
def generate_annot_counts_by_tissue_cat(tiss_info_sdf, annot_abr, feature_type, num_tissue_cat, query_cols, ovrlp_cols, col_tiss, col_cat_id):
	## set up variables:
	tmp_tc_cols = [str(x) for x in range(1, num_tissue_cat+1)]
	
	## create dicts to rename Spark DF columns
	rename_cols_tc_num_ft = dict(zip(tmp_tc_cols, ['tc'+ c +'_' + annot_abr + '_'+ feature_type for c in tmp_tc_cols] ))
	rename_cols_tc_num_tiss = dict(zip(tmp_tc_cols, ['tc'+ c +'_' + annot_abr + '_tiss' for c in tmp_tc_cols] ))
	rename_cols_tc_num_ovrlp = dict(zip(tmp_tc_cols, ['tc'+ c +'_' + annot_abr +'_ovrlp' for c in tmp_tc_cols] ))

	## group sdf by query coord
	query_grp = tiss_info_sdf.groupBy(query_cols)
	
	##################################################################
	## get Tissue Class summary stats
	##################################################################
	## count total # of features (enhancer | eQTL) per Tissue Class per query interval
	tc_num_ft = query_grp.pivot(col_cat_id, [x for x in range(1, num_tissue_cat+1)])\
							.agg(F.count(col_tiss))\
							.orderBy(query_cols)\
							.na.fill(0).cache()
	tc_num_ft = sdf_fxn.rename_spark_columns(tc_num_ft, rename_cols_tc_num_ft)

	## count # of distinct tissues with >=1 overlap per Tissue Class per query interval
	tc_num_tiss = query_grp.pivot(col_cat_id, [x for x in range(1, num_tissue_cat+1)])\
							.agg(F.countDistinct(col_tiss))\
							.orderBy(query_cols)\
							.na.fill(0).cache()
	tc_num_tiss = sdf_fxn.rename_spark_columns(tc_num_tiss, rename_cols_tc_num_tiss)

	## count # of overlap intervals per Tissue Class per query interval
	tc_num_ovrlp = query_grp.pivot(col_cat_id, [x for x in range(1, num_tissue_cat+1)])\
							.agg(F.countDistinct(*ovrlp_cols))\
							.orderBy(query_cols)\
							.na.fill(0).cache()
	tc_num_ovrlp = sdf_fxn.rename_spark_columns(tc_num_ovrlp, rename_cols_tc_num_ovrlp)

	##################################################################
	## join TC_num_features, TC_num_tiss & TC_num_overlaps Spark DFs:
	##################################################################
	## specify column order for joined TC summary stat sdf:
	cols_tc_combined_nested = [ [tc_num_ft.columns[i], tc_num_tiss.columns[i], tc_num_ovrlp.columns[i]] for i in range(3, len(tc_num_ovrlp.columns)) ]
	cols_tc_stat = query_cols + [y for x in cols_tc_combined_nested for y in x]
	
	tc_stat_sdf = tc_num_ft.join(tc_num_tiss, on=query_cols)\
							.join(tc_num_ovrlp, on=query_cols)\
							.select(cols_tc_stat)\
							.orderBy(query_cols).cache()

	##################################################################
	## get Query Interval Totals summary stats:
	##################################################################
	totals_sdf = tiss_info_sdf.groupBy(query_cols)\
								.agg(F.count(col_tiss).alias("Total_#_overlaps"),
									F.countDistinct(col_tiss).alias("Total_#_tissue"),
									F.countDistinct(*ovrlp_cols).alias("Total_#_ovrlp_coord"),
									F.countDistinct(col_cat_id).alias("Total_#_tiss_cat") )\
								.cache()
	totals_sdf = totals_sdf.orderBy(query_cols, inplace=True)

	##################################################################
	## join interval totals with Tissue Category count sDFs:
	##################################################################
	tiss_cat_counts_sdf = totals_sdf.join(tc_stat_sdf, on=query_cols).orderBy(query_cols)

	## unpersist Spark DFs in cache:
	tc_num_ft.unpersist()
	tc_num_tiss.unpersist()
	tc_num_ovrlp.unpersist()
	tc_stat_sdf.unpersist()
	totals_sdf.unpersist()
	
	return tiss_cat_counts_sdf







##################################################################################################
## Step 6: annotation counts & summary stats
##################################################################################################


#### Step 6.a) annotation level summary stats (Totals & by Annotation feature name)
#NOTE: assuming duplicates have been removed from overlap sDF!!!
def generate_annot_total_summary(annot_type, annot_var_dict, ovrlp_sdf, query_cols, ft_cols, ovrlp_cols, spark_context):
	print("\n\nRunning generate_annot_total_summary function:")
	
	## set up variables
	stat_cols = ['#_ft_coord_with_>1_ft_name', '%_ft_coord_with_>1_ft_name',
				  '#_ovrlp_coord_with_>1_ft_name', '%_ovrlp_coord_with_>1_ft_name',
				  '#_ovrlp_coord_with_>1_ft_coord', '%_ovrlp_coord_with_>1_ft_coord',
				  '#_ovrlp_coord_with_>1_query_coord', '%_ovrlp_coord_with_>1_query_coord']

	## 1) calculate overall Totals
	print("\t 1) calculate overall totals")
	total_sdf = ovrlp_sdf.groupBy()\
						.agg(F.countDistinct('ft_name').alias('#_distinct_ft_names'),
							 F.countDistinct(*(['ft_name'] + ft_cols)).alias('Total_#_ft'),
							 F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							 F.countDistinct(*query_cols).alias('#_distinct_query_coord'))
	total_cols_select = total_sdf.columns + stat_cols

	## 2) TMP sdf for calculating per ft_coord
	print("\t 2) count # feature names per feature coord")
	ft_coord = ovrlp_sdf.select(ft_cols + ['ft_name'])\
						.dropDuplicates()\
						.groupBy(ft_cols)\
						.agg(F.count('*').alias('#_ft_names_per_ft_coord'))
		
	## 3) TMP sdf for calculating per ovrlp_coord
	print("\t 3) counts per overlap coord")
	ovrlp_coord = ovrlp_sdf.select(ovrlp_cols + query_cols + ft_cols + ['ft_name'])\
						.dropDuplicates()\
						.groupBy(ovrlp_cols)\
						.agg(F.countDistinct('ft_name').alias('#_ft_names_per_ovrlp_coord'),
							 F.countDistinct(*ft_cols).alias('#_ft_coord_per_ovrlp_coord'),
							 F.countDistinct(*query_cols).alias('#_query_coord_per_ovrlp_coord'))
		
	## 4) add 4 counts from TMP sDFs
	print("\t 4) add counts to totals sDF")
	total_sdf = total_sdf.withColumn('#_ft_coord_with_>1_ft_name',
							F.lit(ft_coord.filter(ft_coord['#_ft_names_per_ft_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_ft_name',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_ft_names_per_ovrlp_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_ft_coord',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_ft_coord_per_ovrlp_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_query_coord',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_query_coord_per_ovrlp_coord'] > 1).count()))
		
	## 5) calculate % for the 4 counts added above
	print("\t 5) calculate % for each count")
	total_sdf = total_sdf.withColumn('%_ft_coord_with_>1_ft_name',
							F.lit(total_sdf['#_ft_coord_with_>1_ft_name'] / total_sdf['#_distinct_ft_coord']) )\
					.withColumn('%_ovrlp_coord_with_>1_ft_name',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_ft_name'] / total_sdf['#_distinct_ovrlp_coord']))\
					.withColumn('%_ovrlp_coord_with_>1_ft_coord',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_ft_coord'] / total_sdf['#_distinct_ovrlp_coord']))\
					.withColumn('%_ovrlp_coord_with_>1_query_coord',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_query_coord'] / total_sdf['#_distinct_ovrlp_coord']))
		
	## 6) reorder & rename columns
	print("\t 6) rename & reorder columns")
	total_sdf = total_sdf.select(total_cols_select)
	rename_dict =  rename_annotation_column_dict(total_cols_select, annot_var_dict[annot_type]['rename_ft_dict'])
	total_sdf = sdf_fxn.rename_spark_columns(total_sdf, rename_dict)
	
	## 7) transpose sDF
	print("\t 7) transpose sDF")
	total_sdf = sdf_fxn.transpose_spark_df_row(total_sdf.collect()[0], spark_context, "Overall Totals", cast_type="double")
	
	## rename new columns
	total_sdf = total_sdf.withColumnRenamed('key', annot_var_dict[annot_type]['annot_name'])
	
	return total_sdf






#NOTE: assuming duplicates have been removed from overlap sDF!!!
def generate_annot_feature_summ_stats(ovrlp_sdf, query_cols, ft_cols, ovrlp_cols):
	print("\n\nRunning generate_annot_feature_summ_stat function:")
	
	## 1) calc totals per feature name
	print("\t 1) calculate totals per annotation feature name")
	annot_count_sdf = ovrlp_sdf.groupBy('ft_name')\
						.agg(F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							 F.countDistinct(*query_cols).alias('#_distinct_query_coord'))
	
	## 2) calc totals per (feature name && feature coord)
	print("\t 2) groupby ft_name + ft_coord => count # distinct SV & overlap coords")
	ft_count_sdf = ovrlp_sdf.groupBy(['ft_name'] + ft_cols)\
						.agg(F.countDistinct(*query_cols).alias('#_query_per_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_ovrlp_coord_per_ft_coord'))\
						.orderBy('ft_name', ascending=False)
		
	## 3) calc averages per annot feature name
	print("\t 3) calculate averages per annotation feature name")
	ft_stat_sdf = ft_count_sdf.groupBy('ft_name')\
						.agg(F.avg('#_ovrlp_coord_per_ft_coord').alias('#_ovrlp_coord_per_ft_coord.AVG'),
							 F.avg('#_query_per_ft_coord').alias('#_query_per_ft_coord.AVG'))
		
	## 4) join ft name totals & averages Spark DFs
	print("\t 4) join temp sDFs => annotation feature summary stat sDF")
	annot_summ_stat_sdf = annot_count_sdf.join(ft_stat_sdf, on='ft_name', how='inner')
	
	return annot_summ_stat_sdf



##################################################################################################
## Step 7: annotation specific analyses / fxns
##################################################################################################

## Helper fxn
def rank_annot_features(annot_summ_stat_sdf, col_rank, top_n=20):
	ranked_sdf =  annot_summ_stat_sdf.orderBy(col_rank, ascending=False)\
									.limit(top_n)
	return ranked_sdf
	


## example of annot type specific analysis
##### -- can be easily refactored for general use to rank by annot feature name
def generate_homer_rankings(annot_summ_stat_sdf, top_n=20):
	## create column renaming dict:
	rename_dict = {'ft_name': 'Transcription Factor', '#_distinct_ft_coord':'# of distinct binding sites',
					'#_distinct_ovrlp_coord':'# of distinct TFBS-SV overlap intervals',
					'#_distinct_query_coord':'# of distinct query coord'}
	
	## top TF - ranked by # of their tfbs:
	print("HOMER TF ranked by # of distinct tfbs coord")
	rank_num_tfbs = rank_annot_features(annot_summ_stat_sdf.select('ft_name', '#_distinct_ft_coord'), '#_distinct_ft_coord', top_n)
	rank_num_tfbs = sdf_fxn.rename_spark_columns(rank_num_tfbs, rename_dict)
	# rank_num_tfbs.show()
	
	## top TF - ranked by # of overlap coord:
	print("HOMER TF ranked by # of distinct overlap coord")
	rank_num_ovrlp = rank_annot_features(annot_summ_stat_sdf.select('ft_name', '#_distinct_ovrlp_coord'), '#_distinct_ovrlp_coord', top_n)
	rank_num_ovrlp = sdf_fxn.rename_spark_columns(rank_num_ovrlp, rename_dict)
	# rank_num_ovrlp.show()

	## top TF - ranked by # of overlapping SV coords:
	print("HOMER TF ranked by # of distinct SV coord")
	rank_num_sv = rank_annot_features(annot_summ_stat_sdf.select('ft_name', '#_distinct_query_coord'), '#_distinct_query_coord', top_n)
	rank_num_sv = sdf_fxn.rename_spark_columns(rank_num_sv, rename_dict)
	# rank_num_sv.show()
	
	return [(rank_num_tfbs, 'binding_sites'), (rank_num_ovrlp, 'TFBS-query_overlap_intervals'), (rank_num_sv, 'query_coord')]

	
	



##################################################################################################
## Step 8: write output files
##################################################################################################
## helper fxn to rename columns for output files


def write_interval_summary_output_file(summ_sdf, summ_sdf_name, query_cols, out_dir, shell_dir):
	print("running write_interval_summary_output_file fxn")
	
	## 1) specify output file name:
	out_file_name = summ_sdf_name + '_interval_summary.tsv'
	
	## 2) check if summ_sdf contains ArrayType columns --> convert to StringType
	summ_sdf = sdf_fxn.convert_ArrayType_columns(summ_sdf)
	# print(summ_sdf.printSchema())
	
	## 3) specify sort cols
	sort_cols = query_cols.copy()
	chr_col = query_cols[0].split('_')[0] + '_chr'
	sort_cols.remove(chr_col)
	
	## 4) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_sort_by_chr(summ_sdf, chr_col, sort_cols, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	return write_sdf_return_code



#### 8.a) write full annotation output file
def write_sv_full_annot_output_file(sv_type, annot_type, ovrlp_sdf, rename_dict, output_cols, out_dir, shell_dir):
	sort_cols = ['sv_start', 'sv_end'] + [rename_dict['ft_start'], rename_dict['ft_end'], rename_dict['ft_name']]
	
	## 1) specify the output file name:
	if "subj_id" in ovrlp_sdf.columns:
		out_file_name = annot_type + "_" + sv_type + "_full_annotation_per_Subject.txt"
		sort_cols = sort_cols + ['subj_id']
	else:
		out_file_name = annot_type + "_" + sv_type + "_full_annotation_per_SV.txt"
	
	## 2) extract relevant columns & drop duplicates
	full_annot_sdf = ovrlp_sdf.select(output_cols).dropDuplicates()

	## 3) rename relevant columns
	full_annot_sdf = sdf_fxn.rename_spark_columns(full_annot_sdf, rename_dict)

	## 4) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_sort_by_chr(full_annot_sdf, "sv_chr", sort_cols, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	return write_sdf_return_code



#### 8.b) write summary stat output files
def write_sv_summary_stat_output_file(sv_type, annot_type, summ_stat_sdf, summ_stat_name, rename_ft_dict, sort_cols, out_dir, shell_dir):
	## 1) specify output file name:
	out_file_name = annot_type + "_" + sv_type + "_overlap_summary_stats-" + summ_stat_name + ".txt"
	
	## 2) generate column renaming dict
	rename_col_dict = rename_annotation_column_dict(summ_stat_sdf.columns, rename_ft_dict)
	
	## 3) rename Spark DF columns
	summ_stat_sdf = sdf_fxn.rename_spark_columns(summ_stat_sdf, rename_col_dict)
	
	## 4) call function in spark_df_functions.py module --> write summ_stat_sdf output file to disk:
	#### if SV-annot count summ stat --> sort by SV coord
	if 'sv' in summ_stat_name.lower():
		write_sdf_return_code = sdf_fxn.write_sdf_to_text_sort_by_chr(summ_stat_sdf, "sv_chr", ['sv_start', 'sv_end'], out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	#### if annotation total summ stat --> do NOT sort
	elif sort_cols is None:
		write_sdf_return_code = sdf_fxn.write_sdf_to_text(summ_stat_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)

	#### if annotation | Subject summ stat --> sort sDF by non-chromosome column(s)
	else:
		if sort_cols == "ft_name":
			sort_cols = rename_col_dict['ft_name']
		write_sdf_return_code = sdf_fxn.write_sdf_to_text(summ_stat_sdf.orderBy(sort_cols), out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	return write_sdf_return_code


#### Step 8.c) output annotation ranking file
def write_ranking_output_file(annot_type, ranking_sdf, ranking_name, top_n, out_dir, shell_dir):
	out_file_name = annot_type + '_top' + str(top_n) + '_by_#_' + ranking_name + '.txt'
	
	write_sdf_return_code = sdf_fxn.write_sdf_to_text(ranking_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	return write_sdf_return_code








##################################################################################################
## Post Annotation processing join individual annot DFs
##################################################################################################

def join_interval_summary(query_sdf, summary_sdf_list, query_cols):
	print("running join_interval_summary fxn")
	
	## outer join DataFrames
	join_sdf = query_sdf
	for summ_sdf in summary_sdf_list:
		join_sdf = join_sdf.join(summ_sdf, on=query_cols, how='outer')
	
	## fill in missing values
	fill_0_cols = [c for c in join_sdf.columns if 'num_' in c]
	join_sdf = join_sdf.fillna(0, subset=fill_0_cols)
	join_sdf = join_sdf.fillna("None")
	
	return join_sdf


def join_tissue_category_counts(tc_cnt_sdf_list, col_tiss_cat):
	print("running join_tissue_category_counts fxn")
	
	join_sdf = tc_cnt_sdf_list[0]
	for tc_cnt in tc_cnt_sdf_list[1:]:
		join_sdf = join_sdf.join(tc_cnt, on=col_tiss_cat, how='outer')
	
	join_sdf = join_sdf.orderBy(col_tiss_cat)
	
	return join_sdf




######################## Multiway overlap by Tissue category

def two_way_overlap(sdf1, sdf2, cols_join, start1, start2, end1, end2, col_prefix):
	print("running two_way_overlap fxn")
	
	## set up variables
	twoway_start = col_prefix + '_start'
	twoway_end = col_prefix + '_end'
	twoway_len = col_prefix + '_len'
	
	## join overlap DFs
	# join_expr = ((F.least(F.col(end1), F.col(end2))+1 - F.greatest(F.col(start1), F.col(start2))) >=1 )
	join_sdf = sdf1.join(sdf2, on=cols_join, how="inner")
	
	## calc two way overlap coord & length
	join_sdf = join_sdf.withColumn(twoway_start, F.greatest(F.col(start1), F.col(start2)))\
						.withColumn(twoway_end, F.least(F.col(end1), F.col(end2)))
	join_sdf = join_sdf.withColumn(twoway_len, F.col(twoway_end)+1 - F.col(twoway_start))
	# print("count before filter:\t", join_sdf.count())
	
	## use twoway overlap length to filter out non-overlapping pairs
	join_filt_sdf = join_sdf.filter(join_sdf[twoway_len] >= 1)
	# print("count after filter:\t", join_filt_sdf.count())
	return join_filt_sdf


def fantom5_roadmap_overlap(f5_tcm_sdf, rm_tcm_sdf, q_cols, ovrlp_cols):
	#### variable setup
	o_str = ovrlp_cols[0].split('_')[0]
	join_cols = q_cols
	if o_str+'_chr' in f5_tcm_sdf.columns:
		join_cols = join_cols + [o_str+'_chr']
	
	start_end_cols = [o_str+'_start_f5', o_str+'_start_rm', o_str+'_end_f5', o_str+'_end_rm']
	twoway_col_prefix = "f5_rm_ovrlp"
	
	## rename FANTOM5 & Roadmap columns
	rename_f5_dict = {col: col+'_f5' for col in f5_tcm_sdf.columns if col not in join_cols}
	rename_rm_dict = {col: col+'_rm' for col in rm_tcm_sdf.columns if col not in join_cols}
	# print(rename_f5_dict)
	
	f5_tcm_renamed_sdf = sdf_fxn.rename_spark_columns(f5_tcm_sdf, rename_f5_dict)
	rm_tcm_renamed_sdf = sdf_fxn.rename_spark_columns(rm_tcm_sdf, rename_rm_dict)
	
	## two way overlap: FANTOM5 vs Roadmap
	f5_rm_tcm_sdf = two_way_overlap(f5_tcm_renamed_sdf, rm_tcm_renamed_sdf, join_cols, *(start_end_cols + [twoway_col_prefix]))
	
	##TODO: reorder columns
	
	return f5_rm_tcm_sdf



def count_twoway_ovrlp_by_tiss_cat(twoway_sdf, annot_abr1, annot_abr2, col_cat_id, num_tiss_cat, spark_session):
	## setup
	tc_cols = ['tc'+str(x) for x in range(1, num_tiss_cat+1)]
	expr_tc_cnt = [ F.count(F.when((F.col(c+'_'+annot_abr1) != '') & (F.col(c+'_'+annot_abr2) != '') , True)).alias(c.strip('tc')) for (c) in tc_cols ]
	col_cnt = 'num_' + annot_abr1 + '+' + annot_abr2 + '_ovrlps'
	
	## count # of twoway overlaps per Tissue Category
	tc_cnt_sdf = twoway_sdf.agg(*expr_tc_cnt)
	
	## transpose PySpark DF via Pandas
	tc_cnt_sdf_T = tc_cnt_sdf.toPandas().T
	tc_cnt_sdf_T.columns = [col_cnt]
	tc_cnt_sdf_T[col_cat_id] = tc_cnt_sdf_T.index
	tc_cnt_sdf_T = tc_cnt_sdf_T[[col_cat_id, col_cnt]]
	
	## convert back to PySpark DF
	tc_cntT_sdf = spark_session.createDataFrame(tc_cnt_sdf_T)
	
	return tc_cntT_sdf


