#!/usr/bin/python -u

import os, warnings, sys, argparse
warnings.filterwarnings('ignore')

from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import *


import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
import process_interval_annotation as annot

from importlib import reload    ## remove this later
reload(sdf_fxn)    ## remove this later
reload(annot)    ## remove this later



####################################################################
#### GLOBAL VARIABLES:

QUERY_COLS = ["q_chr", "q_start", "q_end"]
FT_COLS = ["ft_chr", "ft_start", "ft_end"]
OVRLP_COLS = ["ovrlp_chr", "ovrlp_start", "ovrlp_end"]
COLS_START_END = ["q_start", "q_end", "ft_start", "ft_end", "ovrlp_start", "ovrlp_end"]

####################################################################



## load input interval file
def load_query_interval_file(file_path, sep, col_list, spark_session):
	## read file --> Spark DataFrame using spark_df_functions module
	query_sdf = sdf_fxn.load_name_columns(file_path, sep, col_list, spark_session)
	return query_sdf




## process an annotation type - withOUT using annot metadata dictionary
def process_annotation(query_sdf, annot_type, gadb_gig_file_str, annot_header, query_cols, ovrlp_cols, spark_session, annot_info_file=None, annot_info_join_col='file', num_tiss_cat=32):
	#### 0) setup variables
	return_sdf_dict = {}
	ft_cols = annot_header[:3]
	
	## use for joining with annot info sdf
	join_col_ft = ft_cols[0].split('_')[0] + "_file"
	
	## use for tissue category aggregation
	tissue_annot_types = ['fantom5', 'gtex', 'roadmap']
	col_tissue = 'tissue_name'
	col_tiss_category = 'tiss_cat_name'
	col_tiss_cat_id = 'tiss_cat_id'
	grp_cols = query_cols + ovrlp_cols
	
	
	#### 1) run GIGGLE search
	gig_result_sdf = annot.run_GIGGLE_search(query_sdf, annot_type, gadb_gig_file_str, spark_session)
	gig_result_sdf.show(3)
	
	
	#### 2) process GIGGLE output
	## parse hit string & rename cols
	ovrlp_sdf = annot.parse_GIGGLE_hit_str(gig_result_sdf, annot_header, query_cols)
	ovrlp_sdf.show(3)
	
	## extract overlap info
	ovrlp_sdf = annot.extract_overlap_coord(ovrlp_sdf, query_cols, ft_cols, ovrlp_cols)
	ovrlp_sdf.show(3)

	# ## OPTIONAL: extract length & overlap proportion of query & feature
	# ovrlp_sdf = annot.extract_overlap_length_data(ovrlp_sdf, *COLS_START_END)
	# ovrlp_sdf.show(3)
	
	
	
	#### 3) add annotation specific information from file
	if annot_info_file is not None:
		#TODO: load from GADB DF - use add annot fxn (not from file)
		ovrlp_sdf = annot.add_annot_info_from_file(ovrlp_sdf, annot_info_file, join_col_ft, annot_info_join_col,  spark_session, sep=',')
		ovrlp_sdf.show(3)
		
	
	
	#### 4) if annotation type has tissue specific info
	if annot_type in tissue_annot_types:
		ovrlp_tiss_cat_sdf = annot.generate_tiss_category_matrix(ovrlp_sdf, query_cols, grp_cols, col_tissue, col_tiss_category, col_tiss_cat_id, num_tiss_cat)
		ovrlp_tiss_cat_sdf.show(3)
		
		## add to results return dict
		return_sdf_dict['overlap_TCM'] = ovrlp_tiss_cat_sdf
		
	
	#### 5) annotation aggregate summary stats
	
	
	#### 6) annotation specific analyses
	
	
	
	#### 7) write output files
	
	
	
	#### 8) call plot functions
	
	
	## add overlap sdf to return dict
	return_sdf_dict['overlap'] = ovrlp_sdf
	
	return return_sdf_dict




def run_annotation(query_sdf, annot_type_list, annot_var_dict, spark_session):
	
	for annot in annot_type_list:
		process_annotation(query_sdf, annot, annot_var_dict)


