#!/usr/bin/python
# -*- coding: utf-8 -*-

import pandas as pd
import ast, os

# from pyspark.sql import SparkSession
# from pyspark.sql.types import *
import pyspark.sql.functions as F

import spark_df_functions as sdf_fxn
import interval_annotation as annot

#---------------------------------------------------------#
##       SV-INFERNO Python functions
#---------------------------------------------------------#


####################################################################
#### GLOBAL VARIABLES:
SV_COLS = ['sv_chr', 'sv_start', 'sv_end']
OVRLP_COLS = ['overlap_chr', 'overlap_start', 'overlap_end']
FT_COLS = ['ft_chr', 'ft_start', 'ft_end', 'ft_name', 'ft_file']
TISSUE_INFO_COLS = ['file', 'tissue_name', 'tissue_id', 'tiss_cat_name', 'tiss_cat_id']
####################################################################


def print_sdf(sdf, nrow=5):
	print(sdf.limit(nrow).toPandas().head(nrow))





###########################################################
#### SV-INFERNO setup & config functions
###########################################################

#### generate annotation type variable dict from input text file:
def generate_annot_variable_dict(annot_table_fp):
	## read in table -> Pandas DF
	annot_df = pd.read_table(annot_table_fp, index_col='annot_type')

	## convert DF --> dict
	variable_dict = annot_df.where((pd.notnull(annot_df)), None).to_dict(orient='index')
	# print(variable_dict)

	## convert dict string to dictionary
	for a in variable_dict:
		if variable_dict[a]['rename_full_annot_dict']:
			variable_dict[a]['rename_full_annot_dict']  = ast.literal_eval(variable_dict[a]['rename_full_annot_dict'])

		if variable_dict[a]['rename_ft_dict']:
			variable_dict[a]['rename_ft_dict']  = ast.literal_eval(variable_dict[a]['rename_ft_dict'])
		
		if variable_dict[a]['annot_header']:
			variable_dict[a]['annot_header']  = ast.literal_eval(variable_dict[a]['annot_header'])

	return variable_dict


def generate_annot_config_dict(annot_table_fp):
	## read in table -> Pandas DF
	annot_df = pd.read_table(annot_table_fp, index_col='annot_type')

	## convert DF --> dict
	variable_dict = annot_df.where((pd.notnull(annot_df)), None).to_dict(orient='index')
	# print(variable_dict)

	## convert dict string to dictionary
	for annot in variable_dict:
		for v in variable_dict[annot]:
			
			if type(variable_dict[annot][v]) is str:
				if ('{' in variable_dict[annot][v]) | ('[' in variable_dict[annot][v]) :
					variable_dict[annot][v] = ast.literal_eval(variable_dict[annot][v])
	
	return variable_dict


def output_file_paths_dict(shell_dir, annot_type, sv_type):
	ws_home = shell_dir.split('/src')[0]
	analysis_results_dir = os.path.join(ws_home, "analysis_results", annot_type + "_overlap")
	analysis_tmp_dir = os.path.join(ws_home, "analysis_tmp")
	
	path_dict = {}
	## output files in 'analysis_results/'
	path_dict['full_annot'] = os.path.join(analysis_results_dir, "tables")
	path_dict['ft_summ_stats'] = os.path.join(analysis_results_dir, "tables")
	path_dict['plots'] = os.path.join(analysis_results_dir, "plots")
	
	## output files in 'analysis_tmp/'
	path_dict['sv_annot_counts'] = os.path.join(analysis_tmp_dir, "annot_counts")
	path_dict['overlap_bed'] = os.path.join(analysis_tmp_dir, "multiway_overlaps", "annot_sv_overlap_bed_files")
	path_dict['overlap_parquet'] = os.path.join(analysis_tmp_dir, "multiway_overlaps", "spark_TCM_parquet")

	##################  TODO REMOVE THESE!!!**
	path_dict['giggle_parsed'] = os.path.join(analysis_tmp_dir, "giggle_parsed", annot_type + "_sv_overlap_parsed_tmp.txt")
	path_dict['full_sv_parquet'] = os.path.join(ws_home, "sv_query", "input_sv_parquet")
	path_dict['distinct_sv_parquet'] = os.path.join(ws_home, "sv_query", "sv_distinct_parquet_" + sv_type)
	path_dict['subj_sv_stat_total_parquet'] = os.path.join(ws_home, "sv_query", "subj_total_sv_stat_parquet" )
	
	##########################################################################
	
	return path_dict



def extract_sv_overlap_data(ovrlp_sdf, sv_distinct_sdf, sv_cols, ovrlp_cols):
	print("\n\nRunning extract_overlap_data function:")
	
	#TODO: drop duplicates from overlap sdf???
	#TODO: decide what to do with HOMER duplicates!!!
	## drop duplicate from ovrlp_sdf (if necessary)
	ovrlp_sdf = ovrlp_sdf.dropDuplicates()
	
	## join with distinct SV sDF & add ft_len
	ovrlp_sdf = sv_distinct_sdf.join(ovrlp_sdf, on=sv_cols, how='inner').withColumn('ft_len', ovrlp_sdf.ft_end+1 - ovrlp_sdf.ft_start)
	
	## extract overlap info:
	ovrlp_sdf = ovrlp_sdf.withColumn(ovrlp_cols[0], ovrlp_sdf.sv_chr)\
				.withColumn(ovrlp_cols[1], F.greatest(ovrlp_sdf.sv_start, ovrlp_sdf.ft_start))\
				.withColumn(ovrlp_cols[2], F.least(ovrlp_sdf.sv_end, ovrlp_sdf.ft_end))
	
	## calculate length of SV-annotation overlap interval
	ovrlp_sdf = ovrlp_sdf.withColumn(ovrlp_cols[0].strip('chr')+"len", ovrlp_sdf.overlap_end+1 - ovrlp_sdf.overlap_start)

	## calculate the proportion of the SV & feature that overlap
	ovrlp_sdf = ovrlp_sdf.withColumn('proportion_of_sv', ovrlp_sdf.overlap_len / ovrlp_sdf.sv_len )\
			.withColumn('proportion_of_ft', ovrlp_sdf.overlap_len / ovrlp_sdf.ft_len )
	return ovrlp_sdf








###########################################################
#### SV specific summary stat functions
###########################################################
## originally from run_annot_summ_stats.py
def generate_sv_annot_counts(ovrlp_sdf, sv_col, ft_col, ovrlp_col):
##### TODO: decide whether or not to Drop Duplicates here:
	ovrlp_sdf = ovrlp_sdf.dropDuplicates()\
					 .orderBy(sv_col) #TODO: remove unnecessary orderBy?
#####################################################################
	
	#TODO?? normalize by SV length???
	sv_summ_stat_sdf = ovrlp_sdf.groupBy(sv_col + ['sv_len'])\
					.agg(F.count('*').alias('Total_#_ft'),
						 F.countDistinct('ft_name').alias('#_distinct_ft_names'),
						 F.countDistinct(*ft_col).alias('#_distinct_ft_coord'),
						 F.countDistinct(*ovrlp_col).alias('#_distinct_ovrlp_coord'))\
					.orderBy(sv_col)\
					.cache()
	# sv_summ_stat_sdf.show(3)
	return sv_summ_stat_sdf



def generate_sv_annot_total_summary(sv_type, annot_type, annot_var_dict, ovrlp_sdf, sv_cols, ft_cols, ovrlp_cols, spark_context):
	print("\n\nRunning generate_annot_total_summary function:")
	
	## set up variables
	stat_cols = ['#_ft_coord_with_>1_ft_name', '%_ft_coord_with_>1_ft_name',
				  '#_ovrlp_coord_with_>1_ft_name', '%_ovrlp_coord_with_>1_ft_name',
				  '#_ovrlp_coord_with_>1_ft_coord', '%_ovrlp_coord_with_>1_ft_coord',
				  '#_ovrlp_coord_with_>1_sv_coord', '%_ovrlp_coord_with_>1_sv_coord']

	## 1) calculate overall Totals
	print("\t 1) calculate overall totals")
	total_sdf = ovrlp_sdf.groupBy()\
						.agg(F.countDistinct('ft_name').alias('#_distinct_ft_names'),
							 F.countDistinct(*(['ft_name'] + ft_cols)).alias('Total_#_ft'),
							 F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							 F.countDistinct(*sv_cols).alias('#_distinct_sv_coord'))
	# total_sdf.show(3)
	
	total_cols_select = total_sdf.columns + stat_cols

	## 2) TMP sdf for calculating per ft_coord
	print("\t 2) count # feature names per feature coord")
	ft_coord = ovrlp_sdf.select(ft_cols + ['ft_name'])\
						.dropDuplicates()\
						.groupBy(ft_cols)\
						.agg(F.count('*').alias('#_ft_names_per_ft_coord'))
	# ft_coord.show(3)
	
	## 3) TMP sdf for calculating per ovrlp_coord
	print("\t 3) counts per overlap coord")
	ovrlp_coord = ovrlp_sdf.select(ovrlp_cols + sv_cols + ft_cols + ['ft_name'])\
						.dropDuplicates()\
						.groupBy(ovrlp_cols)\
						.agg(F.countDistinct('ft_name').alias('#_ft_names_per_ovrlp_coord'),
							 F.countDistinct(*ft_cols).alias('#_ft_coord_per_ovrlp_coord'),
							 F.countDistinct(*sv_cols).alias('#_sv_coord_per_ovrlp_coord'))
	# ovrlp_coord.show(3)
	
	## 4) add 4 counts from TMP sDFs
	print("\t 4) add counts to totals sDF")
	total_sdf = total_sdf.withColumn('#_ft_coord_with_>1_ft_name',
							F.lit(ft_coord.filter(ft_coord['#_ft_names_per_ft_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_ft_name',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_ft_names_per_ovrlp_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_ft_coord',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_ft_coord_per_ovrlp_coord'] > 1).count()))\
					.withColumn('#_ovrlp_coord_with_>1_sv_coord',
							F.lit(ovrlp_coord.filter(ovrlp_coord['#_sv_coord_per_ovrlp_coord'] > 1).count()))
	# total_sdf.show(3)
	
	## 5) calculate % for the 4 counts added above
	print("\t 5) calculate % for each count")
	total_sdf = total_sdf.withColumn('%_ft_coord_with_>1_ft_name',
							F.lit(total_sdf['#_ft_coord_with_>1_ft_name'] / total_sdf['#_distinct_ft_coord']) )\
					.withColumn('%_ovrlp_coord_with_>1_ft_name',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_ft_name'] / total_sdf['#_distinct_ovrlp_coord']))\
					.withColumn('%_ovrlp_coord_with_>1_ft_coord',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_ft_coord'] / total_sdf['#_distinct_ovrlp_coord']))\
					.withColumn('%_ovrlp_coord_with_>1_sv_coord',
							F.lit(total_sdf['#_ovrlp_coord_with_>1_sv_coord'] / total_sdf['#_distinct_ovrlp_coord']))
	# total_sdf.show(3)
	
	## 6) reorder & rename columns
	print("\t 6) rename & reorder columns")
	total_sdf = total_sdf.select(total_cols_select)
	rename_dict =  annot.rename_annotation_column_dict(total_cols_select, annot_var_dict[annot_type]['rename_ft_dict'])
	total_sdf = sdf_fxn.rename_spark_columns(total_sdf, rename_dict)
	# total_sdf.show(3)
	
	## 7) transpose sDF
	print("\t 7) transpose sDF")
	total_sdf = sdf_fxn.transpose_spark_df_row(total_sdf.collect()[0], spark_context, "Overall Totals", cast_type="double")
	
	## rename new columns
	total_sdf = total_sdf.withColumnRenamed('key', annot_var_dict[annot_type]['annot_name'] + " " + sv_type)
	total_sdf.show(3)
	
	return total_sdf


## originally from run_annot_summ_stats.py
def generate_annot_feature_summ_stats_perSV(ovrlp_sdf, sv_cols, ft_cols, ovrlp_cols):
	print("\n\nRunning generate_annot_feature_summ_stat_perSV function:")
	
	## 1) calc totals per feature name
	print("\t 1) calculate totals per annotation feature name")
	annot_count_sdf = ovrlp_sdf.groupBy('ft_name')\
						.agg(F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							 F.countDistinct(*sv_cols).alias('#_distinct_sv_coord'))
	# annot_count_sdf.show(3)
	
	## 2) calc totals per (feature name && feature coord)
	print("\t 2) groupby ft_name + ft_coord => count # distinct SV & overlap coords")
	ft_count_sdf = ovrlp_sdf.groupBy(['ft_name'] + ft_cols)\
						.agg(F.countDistinct(*sv_cols).alias('#_sv_per_ft_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_ovrlp_coord_per_ft_coord'))\
						.orderBy('ft_name', ascending=False)
	# ft_count_sdf.show(3)
	
	## 3) calc averages per annot feature name
	print("\t 3) calculate averages per annotation feature name")
	ft_stat_sdf = ft_count_sdf.groupBy('ft_name')\
						.agg(F.avg('#_ovrlp_coord_per_ft_coord').alias('#_ovrlp_coord_per_ft_coord.AVG'),
							 F.avg('#_sv_per_ft_coord').alias('#_sv_per_ft_coord.AVG'))
	# ft_stat_sdf.show(3)
	
	# 4) count # ft & overlap coord per SV (per feature name)
	print("\t 4) groupby ft_name + sv_coord => count # distinct feature & overlap coords")
	ft_count_perSV_sdf = ovrlp_sdf.groupBy(['ft_name'] + sv_cols + ['sv_len'])\
						.agg(F.countDistinct(*ft_cols).alias('#_ft_coord_per_sv_coord'),
							 F.countDistinct(*ovrlp_cols).alias('#_ovrlp_coord_per_sv_coord'))\
						.orderBy('ft_name', ascending=False)
	# ft_count_perSV_sdf.show(3)

	## 5) calc averages per SV, per annot feature name
	print("\t 5) calculate averages per SV, annotation feature name")
	ft_stat_perSV_sdf = ft_count_perSV_sdf.groupBy('ft_name')\
						.agg(F.avg('#_ft_coord_per_sv_coord').alias('#_ft_coord_per_sv_coord.AVG'),
							 F.avg('#_ovrlp_coord_per_sv_coord').alias('#_ovrlp_coord_per_sv_coord.AVG'),
	                         F.avg('sv_len').alias('sv_len.AVG'),
	                         F.min('sv_len').alias('sv_len.MIN'),
	                         F.max('sv_len').alias('sv_len.MAX'),
	                         F.stddev('sv_len').alias('STDDEV.sv_len'))
	# ft_stat_perSV_sdf.show(3)
	
	## 6) join ft name totals & averages Spark DFs
	print("\t 6) join temp sDFs => annotation feature summary stat sDF")
	annot_summ_stat_sdf = annot_count_sdf.join(ft_stat_sdf, on='ft_name', how='inner')\
										.join(ft_stat_perSV_sdf, on='ft_name', how='inner')
	# annot_summ_stat_sdf.show(3)
	
	return annot_summ_stat_sdf









###########################################################
#### Subject level summary stat functions
###########################################################

## originally from run_annot_summ_stats.py
####### if subj_info: join with full subject sv sdf:
#TODO: decide - is this fxn necessary???
def generate_full_subject_overlap_sdf(ovrlp_sdf, sv_subj_sdf, join_cols):
	
	#TODO: #NOTE: assuming duplicates have been removed from ovrlp_sdf
	
	
	#TODO: decide - remove dups from ovrlp_sdf????
	## without duplicates:
	full_annot_subj_sdf = sv_subj_sdf.join(ovrlp_sdf.distinct(), on=join_cols, how='inner').dropDuplicates()
	
	## potentially contains duplicates:
	# full_annot_subj_sdf = sv_subj_sdf.join(ovrlp_sdf, on=join_cols, how='inner')
	return full_annot_subj_sdf



#NOTE: assuming duplicates have been removed from ovrlp_subj_sdf!!!
def subject_annotation_summ_stats(ovrlp_subj_sdf, sv_full_subj_sdf, sv_annot_count_sdf, subj_sv_stats_sdf, sv_cols, ft_cols, ovrlp_cols):
	
	ovrlp_len = ovrlp_cols[0].strip('chr') + 'len'
	reorder_cols = ['subj_id'] + sv_cols + ['sv_len', 'sv_type'] + sv_annot_count_sdf.columns[4:]
	
	#### 1) subject - annot TOTALs:
	#TODO: repartition ovrlp_subj_sdf by subj_id???
	#TODO: normalize by SV length???
	
	subj_total_sdf = ovrlp_subj_sdf.groupBy('subj_id')\
						.agg(F.count('*').alias('Total_#_ft'),
							F.countDistinct('ft_name').alias('#_distinct_ft_names'),
							F.countDistinct(*ft_cols).alias('#_distinct_ft_coord'),
							F.countDistinct(*ovrlp_cols).alias('#_distinct_ovrlp_coord'),
							F.avg('ft_len').alias("ft_len.AVG"),
							F.min('ft_len').alias("ft_len.MIN"),
							F.max('ft_len').alias("ft_len.MAX"),
							F.stddev('ft_len').alias("ft_len.STDDEV"),
							F.avg(ovrlp_len).alias("overlap_len.AVG"),
							F.min(ovrlp_len).alias("overlap_len.MIN"),
							F.max(ovrlp_len).alias("overlap_len.MAX"),
							F.stddev(ovrlp_len).alias("overlap_len.STDDEV"),
							F.avg('proportion_of_ft').alias("proportion_of_ft.AVG"),
							F.min('proportion_of_ft').alias("proportion_of_ft.MIN"),
							F.max('proportion_of_ft').alias("proportion_of_ft.MAX"),
							F.stddev('proportion_of_ft').alias("proportion_of_ft.STDDEV"),
							F.avg('proportion_of_sv').alias("proportion_of_sv.AVG"),
							F.min('proportion_of_sv').alias("proportion_of_sv.MIN"),
							F.max('proportion_of_sv').alias("proportion_of_sv.MAX"),
							F.stddev('proportion_of_sv').alias("proportion_of_sv.STDDEV"))\
						.cache()


	#### 2) subject - annot per SV:
	
	## join full_subj_sv_sdf & sv_annot_count_sdf --> add subj IDs to each SV
	subj_sv_annot_count_sdf = sv_full_subj_sdf.join(sv_annot_count_sdf, on=sv_cols + ['sv_len'], how="inner")\
											.select(reorder_cols)\
											.repartition('subj_id') #TODO: remove repartition to speed up??

	subj_sv_sdf = subj_sv_annot_count_sdf.groupBy(['subj_id'])\
						.agg(F.avg('Total_#_ft').alias('perSV.Total_#_ft.AVG'),
							 F.min('Total_#_ft').alias('perSV.Total_#_ft.MIN'),
							 F.max('Total_#_ft').alias('perSV.Total_#_ft.MAX'),
							 F.stddev('Total_#_ft').alias('perSV.Total_#_ft.STDDEV'),
							 F.avg('#_distinct_ft_names').alias('perSV.#_distinct_ft.AVG'),
							 F.min('#_distinct_ft_names').alias('perSV.#_distinct_ft.MIN'),
							 F.max('#_distinct_ft_names').alias('perSV.#_distinct_ft.MAX'),
							 F.stddev('#_distinct_ft_names').alias('perSV.#_distinct_ft.STDDEV'),
							 F.avg('#_distinct_ft_coord').alias('perSV.#_ft_coord.AVG'),
							 F.min('#_distinct_ft_coord').alias('perSV.#_ft_coord.MIN'),
							 F.max('#_distinct_ft_coord').alias('perSV.#_ft_coord.MAX'),
							 F.stddev('#_distinct_ft_coord').alias('perSV.#_ft_coord.STDDEV'),
							 F.avg('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.AVG'),
							 F.min('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.MIN'),
							 F.max('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.MAX'),
							 F.stddev('#_distinct_ovrlp_coord').alias('perSV.#_ovrlp_coord.STDDEV'))\
						.cache()


	#### 3) join with Subject SV total sDF:
	subj_stat_sdf = subj_sv_stats_sdf.join(subj_total_sdf, on='subj_id', how="inner")\
									.join(subj_sv_sdf, on='subj_id', how="inner")\
									.orderBy('subj_id')\
									.cache()

	return subj_stat_sdf







##################################################################################################
## Step 7: write output files
##################################################################################################


#### 7.a) write full annotation output file
def write_full_annot_output_file(sv_type, annot_type, ovrlp_sdf, rename_dict, output_cols, out_dir, shell_dir):
	sort_cols = ['sv_start', 'sv_end'] + [rename_dict['ft_start'], rename_dict['ft_end'], rename_dict['ft_name']]
	
	## 1) specify the output file name:
	if "subj_id" in ovrlp_sdf.columns:
		out_file_name = annot_type + "_" + sv_type + "_full_annotation_per_Subject.txt"
		sort_cols = sort_cols + ['subj_id']
	else:
		out_file_name = annot_type + "_" + sv_type + "_full_annotation_per_SV.txt"
	
	## 2) extract relevant columns & drop duplicates
	full_annot_sdf = ovrlp_sdf.select(output_cols).dropDuplicates()

	## 3) rename relevant columns
	full_annot_sdf = sdf_fxn.rename_spark_columns(full_annot_sdf, rename_dict)

	## 4) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_sort_by_chr(full_annot_sdf, "sv_chr", sort_cols, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	return write_sdf_return_code



#### 7.b) write summary stat output files
def write_summary_stat_output_file(sv_type, annot_type, summ_stat_sdf, summ_stat_name, rename_ft_dict, sort_cols, shell_dir, out_dir):
	## 1) specify output file name:
	out_file_name = annot_type + "_" + sv_type + "_overlap_summary_stats-" + summ_stat_name + ".txt"
	
	## 2) generate column renaming dict
	rename_col_dict = annot.rename_annotation_column_dict(summ_stat_sdf.columns, rename_ft_dict)
	
	## 3) rename Spark DF columns
	summ_stat_sdf = sdf_fxn.rename_spark_columns(summ_stat_sdf, rename_col_dict)
	
	## 4) call function in spark_df_functions.py module --> write summ_stat_sdf output file to disk:
	#### if SV-annot count summ stat --> sort by SV coord
	if 'sv' in summ_stat_name.lower():
		write_sdf_return_code = sdf_fxn.write_sdf_to_text_sort_by_chr(summ_stat_sdf, "sv_chr", ['sv_start', 'sv_end'], out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	#### if annotation total summ stat --> do NOT sort
	elif sort_cols is None:
		write_sdf_return_code = sdf_fxn.write_sdf_to_text(summ_stat_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)

	#### if annotation | Subject summ stat --> sort sDF by non-chromosome column(s)
	else:
		if sort_cols == "ft_name":
			sort_cols = rename_col_dict['ft_name']
		write_sdf_return_code = sdf_fxn.write_sdf_to_text(summ_stat_sdf.orderBy(sort_cols), out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	return write_sdf_return_code


#### Step 7.c) output annotation ranking file
def write_ranking_output_file(annot_type, ranking_sdf, ranking_name, top_n, shell_dir, out_dir):
	out_file_name = annot_type + '_top' + str(top_n) + '_by_#_' + ranking_name + '.txt'
	
	write_sdf_return_code = sdf_fxn.write_sdf_to_text(ranking_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	return write_sdf_return_code



#------------------------------------------------------------------------------#
#TODO: temporary --> remove these!
#### Step 7.x) output overlap bed file for multiway overlap
def write_sv_annot_overlap_bed_file(sv_type, annot_type, ovrlp_sdf, ovrlp_cols, out_dir, shell_dir):
	"""Function to generate SV-annotation overlap bed file from overlap Spark DF

	:param sdf:
	:param annot_type:
	:param out_dir:
	:param shell_dir:
	:return:
	"""
	bed_file_name = sv_type + "_" + annot_type + "_sv_overlap"
	
	bed_sdf = ovrlp_sdf.select(ovrlp_cols+['ft_name'])\
					.dropDuplicates()\
					.withColumn('annot_type_ft_name',
								F.concat( F.lit(annot_type + "_"), ovrlp_sdf.ft_name ))\
					.select(ovrlp_cols + ['annot_type_ft_name'])
	
	## call function in spark_df_functions.py module --> write bed file
	write_bed_return_code = sdf_fxn.write_sdf_to_bed_file(bed_sdf, out_dir, bed_file_name, shell_dir)
	return write_bed_return_code


#### 7.x write overlap sdf to parquet file
def write_overlap_sdf_to_disk(sv_type, annot_type, ovrlp_sdf, ovrlp_cols, out_dir):
	parquet_dir_name = annot_type + "_" + sv_type + "_overlap_parquet"
	
	## call function in spark_df_functions.py module --> write overlap TCM sdf to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_parquet(ovrlp_sdf, out_dir, parquet_dir_name, compression="gzip", partition_cols=ovrlp_cols[0])
	return write_sdf_return_code
#------------------------------------------------------------------------------#





###########################################################
#### "driver" functions to run annot
###########################################################




def process_sv_giggle_results(annot_type, annot_var_dict, path_dict, overlap_sdf, sv_distinct_sdf, sv_subj_sdf, subj_sv_stat_sdf, shell_script_dir, spark, sv_type="DEL", subj_info=False, write_subj=False, spark_context=None):
	######################### variable setup
	full_annot_cols_dict = annot_var_dict[annot_type]['rename_full_annot_dict']
	rename_ft_dict = dict(full_annot_cols_dict, **annot_var_dict[annot_type]['rename_ft_dict'])
	
	if not annot_var_dict[annot_type]['feature_name_col']:
		FT_COLS.remove('ft_name')
	
	full_annot_cols = SV_COLS + ['num_subj'] + FT_COLS[:-1] + OVRLP_COLS + [OVRLP_COLS[0].strip('chr') + 'len',  'sv_len', 'ft_len', 'proportion_of_sv', 'proportion_of_ft']
	full_annot_cols_subj = SV_COLS + ['num_subj', 'subj_id'] + FT_COLS[:-1] + OVRLP_COLS + [OVRLP_COLS[0].strip('chr') + 'len',  'sv_len', 'ft_len', 'proportion_of_sv', 'proportion_of_ft']
	
	
	######################### process GIGGLE results --> sv-annot overlap sDF
	overlap_sdf = extract_sv_overlap_data(overlap_sdf, sv_distinct_sdf, SV_COLS, OVRLP_COLS)
	print_sdf(overlap_sdf)
	
	#TODO: add annotation specific info (e.g. add tissue file!)
	
	
	
	######################### summary stats:
	## A) SV level summary stats:
	sv_annot_count_sdf = generate_sv_annot_counts(overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS)
	print_sdf(sv_annot_count_sdf)

	## B) annotation level summary stats:
	annot_summ_stat_total_sdf = annot.generate_annot_total_summary(sv_type, annot_type, annot_var_dict, overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS, spark_context)
	print_sdf(annot_summ_stat_total_sdf)

	annot_summ_stat_sdf = annot.generate_annot_feature_summ_stats(overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS)
	print_sdf(annot_summ_stat_sdf)


	## C) [optional] subject level summary stats (IFF input dataset has subject IDs!!!):
	if subj_info:
		full_annot_subj_sdf = generate_full_subject_overlap_sdf(overlap_sdf, sv_subj_sdf, SV_COLS + ['sv_type', 'sv_len'])
		print("\n\n\nfull annot subj sdf: ")
		print_sdf(full_annot_subj_sdf)

		if write_subj:
			returncode_full_annot_per_subj = write_full_annot_output_file(sv_type, annot_type, full_annot_subj_sdf, full_annot_cols_dict, full_annot_cols_subj.copy(), path_dict['full_annot'], shell_script_dir)

		subj_summ_stats_sdf = subject_annotation_summ_stats(full_annot_subj_sdf, sv_subj_sdf, sv_annot_count_sdf, subj_sv_stat_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS)
		print_sdf(subj_summ_stats_sdf)



	######################### write output files: #TODO add return codes to log files
	returncode_full_annot_per_sv = write_full_annot_output_file(sv_type, annot_type, overlap_sdf, full_annot_cols_dict, full_annot_cols.copy(), path_dict['full_annot'], shell_script_dir)

	returncode_write_SV_annot_count_output = write_summary_stat_output_file(sv_type, annot_type, sv_annot_count_sdf, "SV", rename_ft_dict, None, shell_script_dir, path_dict['ft_summ_stats'])

	returncode_write_annot_summStat_output = write_summary_stat_output_file(sv_type, annot_type, annot_summ_stat_sdf, "annotation", rename_ft_dict, 'ft_name', shell_script_dir, path_dict['ft_summ_stats'])

	returncode_write_annot_summStatTOTALs_output = write_summary_stat_output_file(sv_type, annot_type, annot_summ_stat_total_sdf, "annotation_totals", rename_ft_dict, None, shell_script_dir, path_dict['ft_summ_stats'])

	if subj_info:
		returncode_write_Subj_summStat_output = write_summary_stat_output_file(sv_type, annot_type, subj_summ_stats_sdf, "Subject", rename_ft_dict, 'subj_id', shell_script_dir, path_dict['ft_summ_stats'])




