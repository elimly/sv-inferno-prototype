#!/usr/bin/env bash 
TMPDIR=$1
OUTDIR=$2
FILENAME=$3


## move & rename new output file & delete tmp spark outout directory
mv ${TMPDIR}/part* ${OUTDIR}/${FILENAME}
rm -r ${TMPDIR}
