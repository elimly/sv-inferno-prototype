#!/usr/bin/env bash 
TMPDIR=$1
OUTDIR=$2
BEDFILE=$3

## step 1: add .bed extension to file name (if needed):
if [[ $BEDFILE = *".bed" ]]; then
	BEDFILE_NAME=${BEDFILE}
	echo "      CONTAINS .bed  " $BEDFILE_NAME
else
	BEDFILE_NAME=${BEDFILE}.bed
	echo "      adding .bed to file name: " $BEDFILE_NAME
fi


## step 2: sort spark output file
if [ "$(uname)" == "Darwin" ]; then
	gsort -k1,1 -k2,2 -k3,3 -V -s ${TMPDIR}/part* -o ${TMPDIR}/${BEDFILE_NAME}

else
	sort -k1,1 -k2,2 -k3,3 -V -s ${TMPDIR}/part* -o ${TMPDIR}/${BEDFILE_NAME}
fi


## step 3: move new sorted output file & delete tmp spark outout directory
mv ${TMPDIR}/${BEDFILE_NAME} ${OUTDIR}/${BEDFILE_NAME}
rm -r ${TMPDIR}


