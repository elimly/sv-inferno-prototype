
import os, sys, subprocess, warnings
warnings.filterwarnings('ignore')

from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F

import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
import sv_inferno_functions as sv

from importlib import reload ## remove this later
reload(sdf_fxn)
reload(sv)

####################################################################
#### GLOBAL VARIABLES:

INPUT_SV_SCHEMA = StructType([
	StructField("sv_chr", StringType(), True),
	StructField("sv_start", IntegerType(), True),
	StructField("sv_end", IntegerType(), True),
	StructField("sv_type", StringType(), True),
	StructField("subj_id", StringType(), True)])

####################################################################



def input_sv_file_to_sdf(input_sv_file, schema, subj_info=True):
	"""Function to generate input SV Spark DF from input SV file

	:param input_sv_file:
	:return:
	"""
	## 1) check if input file contains header row:
	with open(input_sv_file) as f:
		line0 = f.readline().lower()
	f.close()
	
	if ("start" in line0) | ("end" in line0):
		has_header = True
	else:
		has_header = False
	
	## 2) read file --> Spark DataFrame
	input_sv_sdf = spark.read.csv(input_sv_file, header=has_header,
								  schema=schema, sep='\t')
	
	## 3) sort the dataframe by SV coords
	input_sv_sdf = sdf_fxn.sort_sdf_by_chr(input_sv_sdf, "sv_chr", ["sv_start", "sv_end"])
	
	## 4) add sv_len column:
	#TODO: double check - are coords inclusive / exclusive? end+1? or just subtract?
	input_sv_sdf = input_sv_sdf.withColumn('sv_len', input_sv_sdf.sv_end+1 - input_sv_sdf.sv_start)
	
	## 5) reorder columns & repartition sdf by chromosome:
	reorder_cols = ['sv_chr', 'sv_start', 'sv_end', 'sv_type', 'sv_len']
	if subj_info:
		reorder_cols = reorder_cols + ['subj_id']
	
	input_sv_sdf = input_sv_sdf.select(reorder_cols).repartition('sv_chr')

	return input_sv_sdf



def collapse_input_sv_sdf(input_sdf, subj_info=False):
	"""Function that collapses input SV sdf into distinct SV intervals Spark DF

	:param sdf:
	:return:
	"""
	cols_output = ['sv_chr', 'sv_start', 'sv_end', 'sv_type', 'sv_len']
	if subj_info:
		cols_output = cols_output + ['num_subj']
	
	## drop duplicates, groupBy SV coord & type -> count # subj
	collapsed_sdf = input_sdf.dropDuplicates()\
							.groupBy(['sv_chr', 'sv_start', 'sv_end', "sv_len", 'sv_type'])\
							.agg(F.count('*').alias("num_subj"))\
							.select(*cols_output)
		
	## sort by sv coord
	collapsed_sdf = sdf_fxn.sort_sdf_by_chr(collapsed_sdf, "sv_chr", ["sv_start", "sv_end"])
	return collapsed_sdf


def subject_sv_totals(sv_subj_sdf):
	## drop duplicates & repartition by subject ID
	tmp_subj_sv_sdf = sv_subj_sdf.dropDuplicates().repartition("subj_id").cache()

	## 1) count by SV type
	subj_sdf_sv_type = tmp_subj_sv_sdf.groupBy(['subj_id', 'sv_type'])\
								.agg(F.count('*').alias("num_sv"),
									 F.sum("sv_len").alias("Total_sv_len"),
									 F.avg("sv_len").alias("sv_len.AVG"),
									 F.min("sv_len").alias("sv_len.MIN"),
									 F.max("sv_len").alias("sv_len.MAX"),
									 F.stddev("sv_len").alias("sv_len.STDDEV"))
	select_cols = ["`"+c+"`" for c in subj_sdf_sv_type.columns]
	print(select_cols)

	## 2) totals for ALL SVs combined
	subj_sdf_all_sv = tmp_subj_sv_sdf.groupBy('subj_id')\
								.agg(F.count('*').alias("num_sv"),
									 F.sum("sv_len").alias("Total_sv_len"),
									 F.avg("sv_len").alias("sv_len.AVG"),
									 F.min("sv_len").alias("sv_len.MIN"),
									 F.max("sv_len").alias("sv_len.MAX"),
									 F.stddev("sv_len").alias("sv_len.STDDEV"))\
								.withColumn("sv_type", F.lit("ALL"))\
								.select(select_cols)

	## 3) concat DFs
	subj_sdf = subj_sdf_sv_type.union(subj_sdf_all_sv)\
							.orderBy(["subj_id", "sv_type"], ascending=[1, 1])\
							.fillna(0)\
							.cache()

	tmp_subj_sv_sdf.unpersist()
	return subj_sdf



def write_collapsed_sv_to_bed(sdf, sv_type, shell_path, data_name=None):
	#TODO? use write_to_text_sort_by_chr function???
	"""Function write minimal sv_collapsed_sdf to bed file

	:param sdf:
	:param data_name:
	:return:
	"""
	output_path = 'sv_query'
	tmp_dir = os.path.join(output_path, 'tmp')
	shell_script = os.path.join(shell_path, 'sort_spark_output_to_bed.sh')
	
	sdf = sdf.withColumn("name", F.concat(sdf.sv_type, F.lit("_"),
	                                      sdf.sv_len, F.lit("_"), sdf.num_subj))
	
	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.select(["sv_chr", "sv_start", "sv_end", "name"])\
		.repartition(1).write.csv(tmp_dir, header="false", sep='\t', mode="overwrite")

	## generate file name
	if data_name is not None:
		bed_name = 'collapsed_sv' + '_' + sv_type + "_" + data_name
	else:
		bed_name = 'collapsed_sv' + '_' + sv_type
	print(bed_name)
	
	## run shell script: 1) sort csv -> bed file, 2) remove tmp directory
	response = subprocess.run(
		[shell_script, tmp_dir, output_path, bed_name], shell=False)

	bed_return_code = response.returncode
	###### TODO: add error checking --> log file
	if bed_return_code == 0:
		print("Spark DF successfully saved to bed file")
	else:
		print("ERROR occurred when trying to save Spark DF to bed file")

	return bed_return_code




#TODO: remove data_name???
def process_sv_input(input_sv_file, schema, output_path, shell_dir, data_name=None, subj_info=True, write_files=False):
	## 1) read input SV file -> Spark DF
	input_sdf = input_sv_file_to_sdf(input_sv_file, schema)
	
	## 2) detect sv types
	sv_type_list = [str(s.sv_type) for s in input_sdf[['sv_type']].distinct().collect()]

	## 3) collapse input SVs --> distinct SV intervals
	sv_collapsed_sdf = collapse_input_sv_sdf(input_sdf, subj_info)
	
	## 4) if subj_info --> generate subj SV totals summ stats:
	if subj_info:
		subj_sv_stat_sdf = subject_sv_totals(input_sdf)
	else:
		subj_sv_stat_sdf = None
	
	#--------------------------------------------------------------------------#
	## **TEMPORARY!!!       #TODO: remove print statements!
	print("\ninput SV sDF")
	sdf_fxn.print_sdf(input_sdf)

	print("\ncollapsed SV sDF")
	sdf_fxn.print_sdf(sv_collapsed_sdf)

	if subj_info:
		print("\ninput SV file contains Subject IDs --> generate subject level summary stats")
		sdf_fxn.print_sdf(subj_sv_stat_sdf)

	print("\nSV types in current dataset = " , sv_type_list)
	#--------------------------------------------------------------------------#
	
	
	#TODO: REMOVE THIS!!!
	###########################################################################
	############# **TEMPORARY!!!  write Spark DFs to parquet files
	if write_files:
		for sv_type in sv_type_list:
			## extract sv_type from collapsed sDF
			sv_type_sdf = sv_collapsed_sdf.filter(sv_collapsed_sdf.sv_type == sv_type)

			## write collapsed SV sdf to bed file
			write_collapsed_sv_to_bed(sv_type_sdf, sv_type, shell_dir, data_name)

			## write collapsed SV sdf to parquet
			sdf_fxn.write_sdf_to_parquet(sv_type_sdf, output_path, "sv_distinct_parquet_" + sv_type)

		## write initial input SV sdf to disk for subsequent use (if needed)
		sdf_fxn.write_sdf_to_parquet(input_sdf, output_path, 'input_sv_parquet')

		## write subj Total SV stat sdf to parquet
		if subj_info:
				sdf_fxn.write_sdf_to_parquet(subj_sv_stat_sdf, output_path, "subj_total_sv_stat_parquet")
	############################################################################
	
	
	return input_sdf, sv_collapsed_sdf, subj_sv_stat_sdf, sv_type_list





def annotate_sv_input(sv_full_sdf, sv_collapsed_sdf, sv_type_list, annot_type_list, subj_sv_stat_sdf=None, subj_info=False, write_subj=False, giggle_file_sdf=None):
	## process each SV type separately:
	for sv_type in sv_type_list:
		
		#TODO: filter sv_full_subj_sdf to only current sv_type --> pass to driver script
		## 1) extract sv_type from collapsed sDF
		sv_type_full_sdf = sv_full_sdf.filter(sv_full_sdf.sv_type == sv_type)
		# sdf_fxn.print_sdf(sv_type_full_sdf)
		
		## 2) extract sv_type from collapsed sDF
		sv_type_collapsed_sdf = sv_collapsed_sdf.filter(sv_collapsed_sdf.sv_type == sv_type)
		# sdf_fxn.print_sdf(sv_type_collapsed_sdf)
		
		## 3) extract sv_type from subject sv stat sDF
		if subj_info: #TODO: TEST ME!!
			sv_type_subj_stat_sdf = subj_sv_stat_sdf.filter(subj_sv_stat_sdf.sv_type == sv_type)
			# sdf_fxn.print_sdf(sv_type_subj_stat_sdf)
		else:
			sv_type_subj_stat_sdf = None
		
		## 3) run each annotation type (for each SV type):
		for annot in annot_type_list:
	############################################################################
			#FIXME - replace with real call
			print(sv_type, annot)
			
			#TODO: create new thread
			#TODO: call annotation driver script - with NEW THREAD!!!

				#TODO: pass to driver script: 1) sv_full_sdf, 2) sv_collapsed_sdf, 3) subj_sv_Stat_sdf, 4) sv_type, annot_type, giggle_file_sdf, annot_var_dict, subj_info, write_subj
	############################################################################
	
	
	return None #FIXME: return list of sdfs???





if __name__ == "__main__":
	###############################################################################
	## ** these will be passed in as args in the future **
	num_tiss_classes = 32
	spark_session_name = 'INFERNO-SV-prototype'
	SUBJ_INFO=True
	WRITE_SUBJ=True
	
	#TODO: generate this list from pipeline config file
	annot_type_list = ["homer", "fantom5", "roadmap"]
	
	input_sv_file = "sv_query/parliament_pass_filtered_dels_Chr1_reformat.bed"
	write_files=True
	
	# input_sv_file = "sv_query/parliament_reformat_Chr1_sv-type_TEST.bed"
	# write_files=False
	
	###############################################################################
	
	#------------------ setup Spark:
	spark = SparkSession.builder.appName(spark_session_name).getOrCreate()
	sc = spark.sparkContext
	
	
	#------------------ setup file paths:
	ws_home = os.getcwd()
	while not os.path.isdir(os.path.join(ws_home, 'src')):
		ws_home = ws_home.rpartition('/')[0]
	sys.path.insert(0, ws_home)
	
	shell_script_dir = os.path.join(ws_home, "src", "spark_output_shell_scripts")
	giggle_list_file = os.path.join(ws_home, "pipeline_config", "annotation_info", "giggle_files.tsv")
	#--------------------------------------------------------------------------#
	
	## 0) setup & configuration
	#TODO: pass annot_var_dict --> annot driver script
	annot_var_file = os.path.join(ws_home, "pipeline_config", "annotation_info", "annotation_variable_input_table.tsv")
	annot_var_dict = sv.generate_annot_variable_dict(annot_var_file)
	print(annot_var_dict)
	
	#TODO: load Giggle file path sdf -> pass to annot driver script
	
	
	
	## 1) process input SV file
	sv_processed = process_sv_input(input_sv_file, INPUT_SV_SCHEMA, "sv_query", shell_script_dir, subj_info=SUBJ_INFO, write_files=write_files)
	sv_full_sdf, sv_collapsed_sdf, subj_sv_stat_sdf, sv_type_list = sv_processed[0], sv_processed[1], sv_processed[2], sv_processed[3]
	
	# sdf_fxn.print_sdf(sv_full_sdf)
	# sdf_fxn.print_sdf(sv_collapsed_sdf)
	# sdf_fxn.print_sdf(subj_sv_stat_sdf)
	# print(sv_type_list)
	
	
	## 2) annotate SV data set
	annot_sdfs = annotate_sv_input(sv_full_sdf, sv_collapsed_sdf, sv_type_list, annot_type_list, subj_sv_stat_sdf=subj_sv_stat_sdf, subj_info=SUBJ_INFO, write_subj=WRITE_SUBJ, giggle_file_sdf=None)

	



