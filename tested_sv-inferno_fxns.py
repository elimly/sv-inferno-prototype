
import os, subprocess, warnings, re, sys
warnings.filterwarnings('ignore')

from collections import defaultdict

from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F

import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
import sv_inferno_functions as sv ## custom python module with SV-INFERNO functions


###############################################################################
## ** Global Vars: these will be passed in as args in the future **
num_tiss_cat = 32
spark_session_name = 'INFERNO-SV-prototype'
###############################################################################

spark = SparkSession.builder.appName(spark_session_name).getOrCreate()
sc = spark.sparkContext


#### Helper function to display Spark DF in Pandas format
def print_sdf(sdf, nrow=5):
	return sdf.limit(nrow).toPandas().head(nrow)




###########################################################################################
##### STEP 1 Fxns
###########################################################################################

## specify the schema of the input SV file
INPUT_SV_SCHEMA = StructType([
	StructField("sv_chr", StringType(), True),
	StructField("sv_start", IntegerType(), True),
	StructField("sv_end", IntegerType(), True),
	StructField("name", StringType(), True),
	StructField("sv_type", StringType(), True)])



def input_sv_file_to_sdf(input_sv_file):
	"""Function to generate input SV Spark DF from input SV file

	:param input_sv_file:
	:return:
	"""
	
	## check if input file contains header row:
	with open(input_sv_file) as f:
		line0 = f.readline().lower()
	f.close()
	
	if ("start" in line0) | ("end" in line0):
		has_header = True
	else:
		has_header = False
	
	## read file --> Spark DataFrame
	input_sv_sdf = spark.read\
						.csv(input_sv_file, header=has_header,
								  schema=INPUT_SV_SCHEMA, sep='\t')
	
	## repartition sdf based on SV coords
	input_sv_sdf = input_sv_sdf.repartition('sv_chr', 'sv_start', 'sv_end')
	
	return input_sv_sdf





def collapse_input_sv_sdf(sdf):
	"""Function that collapses input SV sdf into distinct SV intervals Spark DF

	:param sdf:
	:return:
	"""
	sv_distinct_sdf = sdf[['sv_chr', 'sv_start', 'sv_end']].dropDuplicates()
	return sv_distinct_sdf



def write_collapsed_sv_to_bed(sdf, shell_path, data_name=None):
	"""Function write minimal collapsed_sv_sdf to bed file

	:param sdf:
	:param data_name:
	:return:
	"""
	output_path = 'sv_query'
	tmp_dir = os.path.join(output_path, 'tmp')
	shell_script = os.path.join(shell_path, 'sort_spark_output_to_bed.sh')
	
	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep='\t', mode="overwrite")
	
	## shell script: 1) sort csv -> bed file, 2) remove tmp directory
	if data_name is not None:
		bed_name = 'collapsed_sv' + '_' + data_name
	else:
		bed_name = 'collapsed_sv'
	
	response = subprocess.run(
		[shell_script, tmp_dir, output_path, bed_name], shell=False)
	bed_return_code = response.returncode

	###### TODO: add error checking --> log file
	if bed_return_code == 0:
		print("Spark DF succesfully saved to bed file")
	else:
		print("ERROR occured when trying to save Spark DF to bed file")

	return bed_return_code


###########################################################################################
##### STEP 1 - Alternative approaches
###########################################################################################

def collapse_sv_with_info(sdf):
	grpby_cols = ['sv_chr', 'sv_start',  'sv_end']

	sdf = sdf.withColumn('sv_info', F.concat_ws(';', sdf.name, sdf.sv_type))
	grpby_sdf = sdf.select(grpby_cols + ['sv_info'])\
					.dropDuplicates()\
					.groupBy(grpby_cols)\
					.agg(F.collect_list('sv_info').alias('sv_info'))
	return grpby_sdf



def write_collapsed_sv_info_to_bed(sdf, data_name, shell_path):
	output_path = 'sv_query'
	tmp_dir = os.path.join(output_path, 'tmp')
	shell_script = os.path.join(shell_path, 'sort_spark_output_to_bed.sh')
	
	## convert Spark DF -> RDD & concat row cols as string -> write to text file in tmp output directory (unsorted)
	sdf_as_rdd = sdf.rdd\
					.map(lambda line: '\t'.join([str(x) for x in line]))\
					.repartition(1)\
					.saveAsTextFile(tmp_dir)
	
	## shell script: 1) sort csv -> bed file, 2) remove tmp directory
	bed_name = "collapsed_sv_info" + data_name
	
	bed_return_code = subprocess.run(
		[shell_script, tmp_dir, output_path, bed_name], shell=False)
	

	###### TODO: add error checking --> log file
	if bed_return_code == 0:
		print("Spark DF successfully saved to bed file")
	else:
		print("ERROR occurred when trying to save Spark DF to bed file")

	return bed_return_code



def explode_sv_info_str(sdf, select_col_list):
	
	regex_sub_udf = F.udf(lambda x: re.sub('\'','',x), StringType())

	sdf = sdf.withColumn("sv_info_str_1", F.split(sdf.sv_info, "\[")[1])
	sdf = sdf.withColumn("sv_info_str_2", F.split(sdf.sv_info_str_1, "\]")[0])

	sdf = sdf.withColumn("sv_info_str", regex_sub_udf(sdf.sv_info_str_2) )
	sdf = sdf.withColumn("sv_info_list", F.split(sdf.sv_info_str, ",")
	                                    .cast("array<string>"))
	
	sdf = sdf.withColumn('sv_info', F.explode('sv_info_list'))
	
	sdf = sdf.withColumn('name', F.trim(F.split(sdf.sv_info, ';')[0]))\
			.withColumn('sv_type', F.trim(F.split(sdf.sv_info, ';')[1]))
	
	out_sdf = sdf.select(select_col_list)
	
	return out_sdf







###########################################################################################
##### STEP 5 FXNS
###########################################################################################

## Function to read in overlap table (may need be needed if parsed overlap sdf is passed in from annotation process)
def get_overlap_sdf_from_text_file(input_overlap_DF_file_path):
	## read in file --> create Spark DataFrame
	overlap_sdf = spark.read\
						.csv(input_overlap_DF_file_path, sep='\t', header=True, inferSchema=True)

	## remove the file extension from the file name (if present)
	overlap_sdf = overlap_sdf.withColumn('ft_file', F.regexp_replace('ft_file', '.bed*', ''))\
							.withColumn('ft_file', F.regexp_replace('ft_file', '.txt*', ''))
		
	return overlap_sdf


## extract overlap coord from GIGGLE results sdf
def extract_overlap_coord(parsed_giggle_sdf, col_list):
	parsed_giggle_sdf = parsed_giggle_sdf.withColumn("overlap_chr", parsed_giggle_sdf['sv_chr'])\
						.withColumn("overlap_start",
									F.greatest(parsed_giggle_sdf['sv_start'], parsed_giggle_sdf['ft_start']))\
						.withColumn("overlap_end",
									F.least(parsed_giggle_sdf['sv_end'], parsed_giggle_sdf['ft_end']))\
						.select(col_list)

	return parsed_giggle_sdf




## Function to add tissue info to overlap sdf & groupby overlap coord
def add_tissue_info(overlap_sdf, tiss_info_file, tissue_info_cols):
	tissue_info_cols_all = ['file', 'tissue_name', 'tissue_id', 'tiss_cat_name', 'tiss_cat_id'] + tissue_info_cols
	
	## read in Tissue Info Table file -> Spark DF:
	tiss_info_sdf = spark.read\
						.csv(tiss_info_file, sep=',', header=True, inferSchema=True)

	## Join overlap sdf & tissue info sdf
	ovrlp_tiss_info_sdf = overlap_sdf.join(tiss_info_sdf.select(tissue_info_cols_all),
											 on=F.col('ft_file')==F.col('file'),
											 how='left')\
										.drop('file')
	
	return ovrlp_tiss_info_sdf





# # step 2: convert overlap Spark DF --> Tissue Class Matrix
# **TODO: time TCM functions - which is faster???**
# **TODO - decide: what to use for multi-way overlap???  **
#     - SV-overlap coord?  
#     - entire feature coord?  
#     - +/- flanking region? 
#     - allow user to specify?
#------------------------------------------------------------------#

####################################################################
####  Tissue Class Matrix - APPROACH #1
####################################################################
## TCM helper function
def spark_tissue_class_matrix_udf(num_tissue_cat):
	tcm_fields = [StructField("tc"+str(x), IntegerType(), True) for x in range(1, num_tissue_cat+1)]
	tcm_schema = StructType(tcm_fields)
	
	def covert_cat_id_to_TCM_list(row):
		ddict = defaultdict(int)
		for cid in row:
			ddict[cid] +=1
		tcm_row_list = [ddict.get(x, 0) for x in range(1, num_tissue_cat+1)]
		return tcm_row_list
	
	SPARK_cat_id_to_TCM = F.udf(covert_cat_id_to_TCM_list, tcm_schema)
	return SPARK_cat_id_to_TCM



## Function to generate Tissue Class Matrix Spark DF -- APPROACH #1
def generate_TCM_sdf(ovrlp_tiss_info_sdf, cols_to_group, num_tissue_cat, sv_cols):
	## preprocess & groupBy overlap sdf
	grpby_sdf = ovrlp_tiss_info_sdf.select(cols_to_group + ['tissue_id', 'tiss_cat_id'])\
									.dropDuplicates()\
									.orderBy(cols_to_group + ['tissue_id'])\
									.groupBy(cols_to_group).agg(
										F.collect_list('tissue_id').alias('tissue_list'),
										F.collect_list('tiss_cat_id').alias('cat_list'))
	
	## define Spark UDF that converts list(tiss_cat_id) --> TCM row
	SPARK_tiss_cat_id_to_TCM_udf = spark_tissue_class_matrix_udf(num_tissue_cat)
	
	## generate TCM
	cols_select = cols_to_group + ['tissue_list', 'tcm_list.*']
	tcm_sdf = grpby_sdf.withColumn("tcm_list", SPARK_tiss_cat_id_to_TCM_udf(grpby_sdf.cat_list))\
						.select(cols_select)\
						.orderBy(sv_cols)
	
	return tcm_sdf




####################################################################
####  Tissue Class Matrix - APPROACH #2
####################################################################

## Function to generate Tissue Class Matrix Spark DF -- APPROACH #2
def generate_TCM_sdf2(tiss_info_sdf, cols_to_group, num_tissue_classes, sv_cols):
	tc_cols_rename_dict = dict(zip([str(x) for x in range(1, num_tissue_classes+1)],
								   ['tc'+str(y) for y in range(1, num_tissue_classes+1)]))

	## create groupBy object:
	sv_overlap_grp = tiss_info_sdf.select(cols_to_group + ['tissue_id', 'tiss_cat_id'])\
								.dropDuplicates()\
								.orderBy(['sv_chr', 'sv_start', 'sv_end', 'tissue_id'])\
								.groupBy(cols_to_group)

	## create tissue_list
	tiss_list_sdf = sv_overlap_grp.agg(F.collect_list('tissue_id').alias('tissue_list'))\
								.repartition('sv_chr', 'sv_start', 'sv_end')

	## create Tissue Class Matrix columns (values = # of distinct Tissues per TC)
	tc_num_tiss_sdf = sv_overlap_grp.pivot('tiss_cat_id', [x for x in range(1, num_tissue_classes+1)])\
							.agg(F.countDistinct('tissue_id'))\
							.orderBy('sv_chr', 'sv_start', 'sv_end')\
							.na.fill(0)\
							.repartition('sv_chr', 'sv_start', 'sv_end')
	
	tc_num_tiss_sdf = sdf_fxn.rename_spark_columns(tc_num_tiss_sdf, tc_cols_rename_dict)

	## join the Spark DFs on SV
	new_TCM_sdf = tiss_list_sdf.join(tc_num_tiss_sdf, on=cols_to_group, how='outer').orderBy(sv_cols)
	
	return new_TCM_sdf






# #### step 2b: write TCM sdf --> overlap .bed file  

## Function to generate SV-annotation overlap bed file from Tissue Class Matrix Spark DF
def write_sv_annot_overlap_bed_file(tcm_sdf, annot_type, out_dir, shell_dir):
	bed_file_name = annot_type + "_sv_overlap"
	
	## extract overlap coord columns & add column with current annotation type
	tcm_bed_sdf = tcm_sdf[['overlap_chr', 'overlap_start', 'overlap_end']]\
					.dropDuplicates()\
					.withColumn('annot_type', F.lit(annot_type))\
					.select('overlap_chr', 'overlap_start', 'overlap_end', 'annot_type')
	
	## call function in spark_df_functions.py module --> write bed file
	write_bed_return_code = sdf_fxn.write_sdf_to_bed_file(tcm_bed_sdf, out_dir, bed_file_name, shell_dir)

	return write_bed_return_code


## step 2c: write overlap TCM sdf to parquet file  
def write_overlap_TCM_sdf_to_disk(tcm_sdf, annot_type, out_dir):
	parquet_dir_name = annot_type + "_TCM_parquet"
	
	## repartition by overlap coord (will join on overlap cols in multi-way overlap step)
	tcm_sdf.repartition("overlap_chr", "overlap_start", "overlap_end")
	
	## call function in spark_df_functions.py module --> write overlap TCM sdf to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_parquet(tcm_sdf, out_dir, parquet_dir_name, 'gzip')
	
	return write_sdf_return_code



####################################################################
#### step 3: get counts per SV per Tissue Category
####################################################################
#### for each SV interval - SV TOTALS:
# 1) Total # features [enhancers | eQTLs]
# 2) Total # tissues
# 3) Total # Tissue Categories with >= 1 overlap
# 4) Total # of overlaps (distinct)

#### for each SV interval, for each Tissue Category:
# a) # features [enhancers | eQTLs] per Tissue Category
# b) # tissues (distinct) per Tissue Category
# c) # overlap intervals (distinct) per Tissue Category
#------------------------------------------------------------------#


 

def generate_annot_counts_by_tissue_cat_from_TCM(tcm_sdf, num_tissue_cat, ovrlp_tiss_info_sdf):
	"""Function to generate annot counts using the TCM
	
	This fxn is the original approach using the TCM - which has limitations...
	   -the TCM can only encode 1 number!!!
	   - either the # of tissues OR the # of features (i.e. enhancers).
	
	**The # of overlap intervals can be derived from both,
	but all 3 pieces of information cannot be computed from the TCM.

	:param tcm_sdf:
	:param num_tissue_cat:
	:param ovrlp_tiss_info_sdf:
	:return:
	"""
	
	## set up variables to be used
	sv_grp_cols = ['sv_chr', 'sv_start', 'sv_end']
	tc_cols = ['tc'+str(x) for x in range(1, num_tissue_cat+1)]
	sum_col_names = ['sum(' + c + ')' for c in tc_cols]
	cnt_col_names = [c + '_cnt' for c in tc_cols]
	cols_to_sum = [F.col(c) for c in sum_col_names]

	## create dicts to rename Spark DF columns
	rename_cols_tc_sum_num_enh = dict(zip(sum_col_names, [c+'_enh' for c in tc_cols] ))
	rename_cols_tc_cnt_num_overlap = dict(zip(cnt_col_names, [c+'_ovrlp' for c in tc_cols] ))

	## create overlap_TCM_sdf groupby:
	overlap_TCM_grp = tcm_sdf.groupBy(sv_grp_cols)

	#################################################
	## Sum # enhancers (=SUM # tissues per overlap interval) per Tissue Class per SV
	#################################################
	tc_sum_sdf = overlap_TCM_grp.sum(*tc_cols).cache()  ## cache & unpersist?
	tc_sum_sdf = tc_sum_sdf.withColumn("Total_enh_TCM", sum(tc_sum_sdf[c] for c in sum_col_names))
	tc_sum_sdf = sdf_fxn.rename_spark_columns(tc_sum_sdf, rename_cols_tc_sum_num_enh)

	#################################################
	## Count # of overlaps per Tissue Category per SV
	#################################################
	expr_tc_cnt = [ F.count(F.when(F.col(c) > 0, True)).alias(c+'_cnt') for (c) in tc_cols ]

	tc_cnt_sdf = overlap_TCM_grp.agg(*expr_tc_cnt).cache()  ## cache & unpersist?
	tc_cnt_sdf = tc_cnt_sdf.withColumn("num_TC_w_ovrlp",
									   sum([F.when(tc_cnt_sdf[c] > 0, 1).otherwise(0) for c in cnt_col_names]))
	tc_cnt_sdf = sdf_fxn.rename_spark_columns(tc_cnt_sdf, rename_cols_tc_cnt_num_overlap)

	#################################################
	## join TC sum & count sdfs
	#################################################
	## specify column order for joined TC summary stat sdf:
	tc_stat_cols = tc_sum_sdf.columns[3:-1] + tc_cnt_sdf.columns[3:-1]
	tc_stat_cols.sort()
	tc_stat_cols = ['sv_chr', 'sv_start', 'sv_end'] + tc_stat_cols + [tc_sum_sdf.columns[-1], tc_cnt_sdf.columns[-1]]

	## join TC sum(# enhancers) & TC count(# overlaps) Spark DFs:
	tc_stat_sdf = tc_sum_sdf.join(tc_cnt_sdf, on=['sv_chr', 'sv_start', 'sv_end'])\
							.select(tc_stat_cols)

	## remove sum & count sdfs from cached memory:
	tc_sum_sdf.unpersist()
	tc_cnt_sdf.unpersist()

	#################################################
	## get SV totals
	#################################################
	sv_totals = ovrlp_tiss_info_sdf.groupBy(sv_grp_cols)\
								.agg(F.count("tissue_id").alias("Total_enh"),
									 F.countDistinct("tissue_id").alias("Total_tiss"),
									 F.countDistinct("overlap_chr", "overlap_start", "overlap_end").alias("Total_ovrlp"),
									 F.countDistinct("tiss_cat_id").alias("Total_tc") )
	
	sv_totals = sv_totals.orderBy(sv_grp_cols, inplace=True)

	#################################################
	## JOIN  SV totals && TCM_stat
	#################################################
	sv_tissue_counts_sdf = sv_totals.join(tc_stat_sdf, on=sv_grp_cols)\
									.orderBy(sv_grp_cols)

	return sv_tissue_counts_sdf





def generate_annot_counts_by_tissue_cat(tiss_info_sdf, annot_abr, feature_type, num_tissue_cat, sv_cols):
	"""Function to get annotation Counts (Total # of features,
			# distinct tissues & # of overlaps) per TClass per SV
	
	**This fxn calculates all 3 pieces of info - BUT does NOT use the TCM !

	:param tiss_info_sdf:
	:param annot_abr:
	:param feature_type:
	:param num_tissue_cat:
	:param sv_cols:
	:return:
	"""
	## set up variables:
	tmp_tc_cols = [str(x) for x in range(1, num_tissue_cat+1)]
	
	## create dicts to rename Spark DF columns
	rename_cols_tc_num_ft = dict(zip(tmp_tc_cols, ['tc'+ c +'_' + annot_abr + '_'+ feature_type for c in tmp_tc_cols] ))
	rename_cols_tc_num_tiss = dict(zip(tmp_tc_cols, ['tc'+ c +'_' + annot_abr + '_tiss' for c in tmp_tc_cols] ))
	rename_cols_tc_num_ovrlp = dict(zip(tmp_tc_cols, ['tc'+ c +'_' + annot_abr +'_ovrlp' for c in tmp_tc_cols] ))

	## group sdf by SV coord
	sv_grp = tiss_info_sdf.groupBy(sv_cols)
	
	##################################################################
	## get Tissue Class summary stats
	##################################################################
	## count total # of features (enhancer | eQTL) per Tissue Class per SV
	tc_num_ft = sv_grp.pivot('tiss_cat_id', [x for x in range(1, num_tissue_cat+1)])\
					.agg(F.count('tissue_id'))\
					.orderBy(sv_cols)\
					.na.fill(0)\
					.cache()
	tc_num_ft = sdf_fxn.rename_spark_columns(tc_num_ft, rename_cols_tc_num_ft)

	## count # of distinct tissues with >=1 overlap per Tissue Class per SV
	tc_num_tiss = sv_grp.pivot('tiss_cat_id', [x for x in range(1, num_tissue_cat+1)])\
					.agg(F.countDistinct('tissue_id'))\
					.orderBy(sv_cols)\
					.na.fill(0)\
					.cache()
	tc_num_tiss = sdf_fxn.rename_spark_columns(tc_num_tiss, rename_cols_tc_num_tiss)

	## count # of overlap intervals per Tissue Class per SV
	tc_num_ovrlp = sv_grp.pivot('tiss_cat_id', [x for x in range(1, num_tissue_cat+1)])\
					.agg(F.countDistinct('overlap_chr', 'overlap_start', 'overlap_end'))\
					.orderBy(sv_cols)\
					.na.fill(0)\
					.cache()
	tc_num_ovrlp = sdf_fxn.rename_spark_columns(tc_num_ovrlp, rename_cols_tc_num_ovrlp)

	##################################################################
	## join TC_num_features, TC_num_tiss & TC_num_overlaps Spark DFs:
	##################################################################
	## specify column order for joined TC summary stat sdf:
	cols_tc_combined_nested = [ [tc_num_ft.columns[i], tc_num_tiss.columns[i], tc_num_ovrlp.columns[i]] for i in range(3, len(tc_num_ovrlp.columns)) ]
	cols_tc_stat = ['sv_chr', 'sv_start', 'sv_end'] + [y for x in cols_tc_combined_nested for y in x]
	
	tc_stat_sdf = tc_num_ft.join(tc_num_tiss, on=sv_cols)\
							.join(tc_num_ovrlp, on=sv_cols)\
							.select(cols_tc_stat).orderBy(sv_cols).cache()

	##################################################################
	## get SV Totals summary stats:
	##################################################################
	sv_totals = tiss_info_sdf.groupBy(sv_cols)\
					.agg(F.count("tissue_id").alias("Total_enh"),
						 F.countDistinct("tissue_id").alias("Total_tiss"),
						 F.countDistinct("overlap_chr", "overlap_start", "overlap_end").alias("Total_ovrlp"),
						 F.countDistinct("tiss_cat_id").alias("Total_tc") )\
					.cache()
	sv_totals = sv_totals.orderBy(sv_cols, inplace=True)

	##################################################################
	## join SV totals with Tissue Category count sDFs:
	##################################################################
	sv_tissue_counts_sdf = sv_totals.join(tc_stat_sdf, on=sv_cols).orderBy(sv_cols)

	## unpersist Spark DFs in cache:
	tc_num_ft.unpersist()
	tc_num_tiss.unpersist()
	tc_num_ovrlp.unpersist()
	tc_stat_sdf.unpersist()
	sv_totals.unpersist()
	
	return sv_tissue_counts_sdf





## step 3b: write annotation count sdf to output file
def write_annot_count_output_file(sv_count_sdf, annot_type, shell_dir, out_dir):
	## set up variables (refactor these???) & get relative paths:
	out_file_name = annot_type + "_sv_overlap_counts.txt"
	
	## call function in spark_df_functions.py module --> write annot count sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_postsort(sv_count_sdf, out_dir, out_file_name, '\t', True, shell_dir)
	
	return write_sdf_return_code


####################################################################
#### step 4: get Tissue Class totals (for Tissue Class summary file)
####################################################################

def get_tissue_cat_totals(tiss_info_sdf, num_tissue_cat, annot_type, feature_type):
	## create tmp sDF with all Tissue Classes & specify the Annotation & Feature type:
	tmp_df_list = zip([x for x in range(1, num_tissue_cat+1)],
					  [annot_type + '_' + feature_type for x in range(1, num_tissue_cat+1)])
	
	tmp_tclass_sdf = spark.createDataFrame(tmp_df_list, ['tiss_cat_id', 'annotation'])

	## get Tissue Class totals:
	tiss_cat_totals_sdf = tiss_info_sdf.groupBy('tiss_cat_id')\
									.agg(F.count("tissue_id").alias("annot_count"),
										 F.countDistinct("tissue_id").alias("num_tiss_in_TC"),
										 F.countDistinct("overlap_chr", "overlap_start", "overlap_end").alias("num_distinct_ovrlp"))\
									.orderBy('tiss_cat_id')

	## join Tissue Class totals with full list of Tissue Classes (in case 1+ TC doesn't have any overlaps in a data set)
	tc_totals_sdf = tiss_cat_totals_sdf.join(tmp_tclass_sdf, on='tiss_cat_id', how='outer')\
									.na.fill(0)\
									.orderBy('tiss_cat_id')\
									.select('tiss_cat_id', 'annotation', 'annot_count', 'num_tiss_in_TC', 'num_distinct_ovrlp')
	
	return tc_totals_sdf


## write Tissue Class counts sdf to disk
def write_tiss_cat_count_output_file(tc_count_sdf, annot_type, shell_dir, out_dir):
	## set up variables (refactor these???) & get relative paths:
	out_file_name = annot_type + "_tissue_cat_counts.txt"
	
	## call function in spark_df_functions.py module --> write tissue class count sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_postsort(tc_count_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	return write_sdf_return_code





####################################################################
# # step 5: generate FULL annotation output file 
####################################################################

#------------------------------------------------------------------#
# **TODO -  "full_annotation" output file - decide: **
# - only SVs with overlap - for each subject???
#       **or**
# - only SVs with overlap - with SVs COLLAPSED & NO subject info???
#       **or**
# - only SVs with overlap - with SVs COLLAPSED + list & count of subjects???   
#------------------------------------------------------------------#




#### option1: output only SVs that have overlapping annotations - collapsed by SV WITHOUT subject info
def generate_full_annot_output_file_per_SV(tiss_info_sdf, annot_type, rename_dict, shell_dir, out_dir, sv_cols, ft_cols, tiss_info_cols):
	## set up column lists & specify the output file name:
	tmp_annot_cols = sv_cols + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] \
					 + tiss_info_cols + ['overlap_chr', 'overlap_start', 'overlap_end']
	out_file_name = annot_type + "_sv_overlap_full_annotation_per_SV_noStats.txt"
	
	## 1) extract & reorder relevant cols -> sort sDF
	full_annot_sdf = tiss_info_sdf.select(tmp_annot_cols)\
									.orderBy(sv_cols)
	
	## 2) rename columns
	full_annot_sdf = sdf_fxn.rename_spark_columns(full_annot_sdf, rename_dict)

	## for testing - remove later:
	print(full_annot_sdf.count())
#     full_annot_sdf.show(5)
	print_sdf(full_annot_sdf)
	print(print_sdf(full_annot_sdf))
	
#     return full_annot_sdf
	
	
	## 3) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_postsort(full_annot_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	return write_sdf_return_code




def generate_full_annot_output_file_per_SV_with_stats(tiss_info_sdf, annot_type, rename_dict, shell_dir, out_dir, sv_cols, ft_cols, tiss_info_cols):
	## specify the output file name:
	out_file_name = annot_type + "_sv_overlap_full_annotation_per_SV.txt"
	
#################################################################################################
	## 1) add overlap stats - for all annotation types EXCEPT GTEx eQTLs
	if annot_type != "gtex":
		tmp_annot_cols = sv_cols + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] + tiss_info_cols + ['overlap_chr', 'overlap_start', 'overlap_end', 'overlap_len', 'sv_len', 'ft_len', 'proportion_of_sv', 'proportion_of_ft']
		
		tiss_info_sdf = tiss_info_sdf.withColumn('overlap_len', tiss_info_sdf.overlap_end+1 - tiss_info_sdf.overlap_start)\
									.withColumn('sv_len', tiss_info_sdf.sv_end+1 - tiss_info_sdf.sv_start)\
									.withColumn('ft_len', tiss_info_sdf.ft_end+1 - tiss_info_sdf.ft_start)

		tiss_info_sdf = tiss_info_sdf.withColumn('proportion_of_sv', tiss_info_sdf.overlap_len / tiss_info_sdf.sv_len )\
									.withColumn('proportion_of_ft', tiss_info_sdf.overlap_len / tiss_info_sdf.ft_len )

		tiss_info_sdf.show(3)
		
	else:
		tmp_annot_cols = sv_cols + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] \
						 + tiss_info_cols + ['overlap_chr', 'overlap_start', 'overlap_end']

#################################################################################################
	
	## 2) extract & reorder relevant cols -> sort sDF
	full_annot_sdf = tiss_info_sdf.select(tmp_annot_cols).orderBy(sv_cols)
	
	## 3) rename columns
	full_annot_sdf = sdf_fxn.rename_spark_columns(full_annot_sdf, rename_dict)

	## for testing - remove later:
	print(full_annot_sdf.count())
#     full_annot_sdf.show(5)
	print_sdf(full_annot_sdf)
	print(print_sdf(full_annot_sdf))
	
#     return full_annot_sdf
	
	## 4) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_postsort(full_annot_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	return write_sdf_return_code





#### option 2: output SVs for each subject -- decide: include SVs with no annot overlap (outter join with orig query parquet?)
def generate_full_annot_output_file_per_Subject(tiss_info_sdf, annot_type, rename_dict, orig_sv_sdf, shell_dir, out_dir, sv_cols, ft_cols, tiss_info_cols):
	## set up column lists & specify the output file name:
	tmp_annot_cols = sv_cols + ['overlap_chr', 'overlap_start', 'overlap_end'] \
					 + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] + tiss_info_cols
	reorder_cols = orig_sv_sdf.columns + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] \
				   + tiss_info_cols + ['overlap_chr', 'overlap_start', 'overlap_end']
	
	out_file_name = annot_type + "_sv_overlap_full_annotation_per_Subject_noStats.txt"
	
	## 1) join input SV sdf & tissue info overlap sdf
	#### 1.a)select subset of columns to join with original SV input table:
	annot_sdf_tmp = tiss_info_sdf.select(tmp_annot_cols)\
								.orderBy(sv_cols)\
								.cache()

	#### 1.b) join Spark DFs
	full_annot_sdf = orig_sv_sdf.join(annot_sdf_tmp, on=sv_cols, how='inner').select(reorder_cols)
	
	#### 1.c) rename columns:
	full_annot_sdf = sdf_fxn.rename_spark_columns(full_annot_sdf, rename_dict)
	
	#### 1.d) remove cached Spark DFs:
	orig_sv_sdf.unpersist()
	annot_sdf_tmp.unpersist()
	
	
	## 2) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_postsort(full_annot_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	## for testing - remove later:
	print(full_annot_sdf.count())
#     full_annot_sdf.show(3)
	print_sdf(full_annot_sdf)
	print(print_sdf(full_annot_sdf))
	
	return write_sdf_return_code



def generate_full_annot_output_file_per_Subject_with_stats(tiss_info_sdf, annot_type, rename_dict, orig_sv_sdf, shell_dir, out_dir, sv_cols, ft_cols, tiss_info_cols):
	## specify the output file name:
	out_file_name = annot_type + "_sv_overlap_full_annotation_per_Subject.txt"

#################################################################################################
	## 1) add overlap stats - for all annotation types EXCEPT GTEx eQTLs
	if annot_type != "gtex":
		tmp_annot_cols = sv_cols + ['overlap_chr', 'overlap_start', 'overlap_end'] \
						 + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] \
						 + tiss_info_cols + ['overlap_len', 'sv_len', 'ft_len', 'proportion_of_sv', 'proportion_of_ft']
		reorder_cols = orig_sv_sdf.columns + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] \
					   + tiss_info_cols \
					   + ['overlap_chr', 'overlap_start', 'overlap_end', 'overlap_len', 'sv_len', 'ft_len', 'proportion_of_sv', 'proportion_of_ft']
		
		tiss_info_sdf = tiss_info_sdf.withColumn('overlap_len', tiss_info_sdf.overlap_end+1 - tiss_info_sdf.overlap_start)\
									.withColumn('sv_len', tiss_info_sdf.sv_end+1 - tiss_info_sdf.sv_start)\
									.withColumn('ft_len', tiss_info_sdf.ft_end+1 - tiss_info_sdf.ft_start)

		tiss_info_sdf = tiss_info_sdf.withColumn('proportion_of_sv', tiss_info_sdf.overlap_len / tiss_info_sdf.sv_len )\
									.withColumn('proportion_of_ft', tiss_info_sdf.overlap_len / tiss_info_sdf.ft_len )
		
	else:
		tmp_annot_cols = sv_cols + ['overlap_chr', 'overlap_start', 'overlap_end'] \
						 + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] + tiss_info_cols
		reorder_cols = orig_sv_sdf.columns  + ft_cols[:-1] + ['tissue_name', 'tiss_cat_name'] \
					   + tiss_info_cols + ['overlap_chr', 'overlap_start', 'overlap_end']
	
#################################################################################################
	
	## 2) join input SV sdf & tissue info overlap sdf
	#### 2.a)select subset of columns to join with original SV input table:
	annot_sdf_tmp = tiss_info_sdf.select(tmp_annot_cols)\
								.orderBy(sv_cols)\
								.cache()

	#### 2.b) join Spark DFs
	full_annot_sdf = orig_sv_sdf.join(annot_sdf_tmp, on=sv_cols, how='inner').select(reorder_cols)
	
	#### 2.c) rename columns:
	full_annot_sdf = sdf_fxn.rename_spark_columns(full_annot_sdf, rename_dict)
	
	#### 2.d) remove cached Spark DFs:
	orig_sv_sdf.unpersist()
	annot_sdf_tmp.unpersist()
	
	
	## 3) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_postsort(full_annot_sdf, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	## for testing - remove later:
	print(full_annot_sdf.count())
#     full_annot_sdf.show(3)
	print_sdf(full_annot_sdf)
	print(print_sdf(full_annot_sdf))
	
	return write_sdf_return_code




if __name__ == "__main__":
	print("Started. Running in all-spark-notebook Docker container\n")
	

