#!/usr/bin/python -u

import os, subprocess, warnings, sys, argparse, json, ast
warnings.filterwarnings('ignore')
import pandas as pd

from importlib import reload
from itertools import chain

from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F


import spark_df_functions as sdf_fxn ## Spark DF functions custom python module
from sv_annot_summ_stats import *
reload(sdf_fxn)    ## remove this later

def print_sdf(sdf, nrow=5):
	print(sdf.limit(nrow).toPandas().head(nrow))


####################################################################
#### GLOBAL VARIABLES:
SV_COLS = ['sv_chr', 'sv_start', 'sv_end']
OVRLP_COLS = ['overlap_chr', 'overlap_start', 'overlap_end']
FT_COLS = ['ft_chr', 'ft_start', 'ft_end', 'ft_name', 'ft_file']
TISSUE_INFO_COLS = ['file', 'tissue_name', 'tissue_id', 'tiss_cat_name', 'tiss_cat_id']
####################################################################


##################################################################################################
## ** "driver" fxn that runs the program:
##################################################################################################
def process_giggle_results(annot_type, annot_var_dict, path_dict, overlap_sdf, sv_distinct_sdf, sv_subj_sdf, subj_sv_stat_sdf, shell_script_dir, spark, sv_type="DEL", subj_info=False, write_subj=False, spark_context=None):
	######################### variable setup
	full_annot_cols_dict = annot_var_dict[annot_type]['rename_full_annot_dict']
	rename_ft_dict = dict(full_annot_cols_dict, **annot_var_dict[annot_type]['rename_ft_dict'])
	
	if not annot_var_dict[annot_type]['feature_name_col']:
		FT_COLS.remove('ft_name')
	
	full_annot_cols = SV_COLS + ['num_subj'] + FT_COLS[:-1] + OVRLP_COLS + [OVRLP_COLS[0].strip('chr') + 'len',  'sv_len', 'ft_len', 'proportion_of_sv', 'proportion_of_ft']

	
	
	######################### process GIGGLE results --> sv-annot overlap sDF
	overlap_sdf = extract_overlap_data(overlap_sdf, SV_COLS, FT_COLS, OVRLP_COLS)
	print_sdf(overlap_sdf)
	
	#TODO: add annotation specific info (e.g. add tissue file!)
	
	
	
	######################### summary stats:
	## A) SV level summary stats:
	# sv_annot_count_sdf = generate_sv_annot_counts(overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS)
	# print_sdf(sv_annot_count_sdf)
	
	## B) annotation level summary stats:
	annot_summ_stat_total_sdf = generate_annot_total_summary(sv_type, annot_type, annot_var_dict, overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS, spark_context)
	print_sdf(annot_summ_stat_total_sdf)
	
	annot_summ_stat_sdf = generate_annot_feature_summ_stats(overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS)
	print_sdf(annot_summ_stat_sdf)
	
	
	
	######################### write output files: #TODO add return codes to log files
	returncode_full_annot_per_sv = write_full_annot_output_file(sv_type, annot_type, overlap_sdf, full_annot_cols_dict, full_annot_cols.copy(), path_dict['full_annot'], shell_script_dir)

	# returncode_write_SV_annot_count_output = write_summary_stat_output_file(sv_type, annot_type, sv_annot_count_sdf, "SV", rename_ft_dict, None, shell_script_dir, path_dict['ft_summ_stats'])

	returncode_write_annot_summStat_output = write_summary_stat_output_file(sv_type, annot_type, annot_summ_stat_sdf, "annotation", rename_ft_dict, 'ft_name', shell_script_dir, path_dict['ft_summ_stats'])

	returncode_write_annot_summStatTOTALs_output = write_summary_stat_output_file(sv_type, annot_type, annot_summ_stat_total_sdf, "annotation_totals", rename_ft_dict, None, shell_script_dir, path_dict['ft_summ_stats'])
	


	




def test_annot_summ_stats(annot_type, annot_var_dict, path_dict, overlap_sdf,  shell_script_dir, spark,  spark_context=None, write_output=False):
	######################### variable setup
	full_annot_cols_dict = annot_var_dict[annot_type]['rename_full_annot_dict']
	rename_ft_dict = dict(full_annot_cols_dict, **annot_var_dict[annot_type]['rename_ft_dict'])
	
	
	######################### process GIGGLE results --> sv-annot overlap sDF
	overlap_sdf = extract_overlap_data(overlap_sdf, SV_COLS, FT_COLS, OVRLP_COLS)
	print_sdf(overlap_sdf)
	
	
	######################### annotation level summary stats:
	# annot_summ_stat_total_sdf = generate_annot_total_summary(sv_type, annot_type, annot_var_dict, overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS, spark_context)
	# annot_summ_stat_total_sdf.show(3)
	# print_sdf(annot_summ_stat_total_sdf)
	
	annot_summ_stat_sdf = generate_annot_feature_summ_stats(overlap_sdf, SV_COLS, FT_COLS[:3], OVRLP_COLS)
	annot_summ_stat_sdf.show(3)
	
	top_n = 20
	homer_rankings = generate_homer_rankings(annot_summ_stat_sdf, top_n)
	
	
	######################### write output files:
	if write_output:
		for ranking in homer_rankings:
			write_ranking_output_file(annot_type, ranking[0], ranking[1], top_n, shell_script_dir, path_dict['ft_summ_stats'])
		
		print("\n\nwriting output files...")
		# returncode_write_annot_summStat_output = write_summary_stat_output_file(sv_type, annot_type, annot_summ_stat_sdf, "annotation", rename_ft_dict, 'ft_name', shell_script_dir, path_dict['ft_summ_stats'])
		#
		# returncode_write_annot_summStatTOTALs_output = write_summary_stat_output_file(sv_type, annot_type, annot_summ_stat_total_sdf, "annotation_totals", rename_ft_dict, None, shell_script_dir, path_dict['ft_summ_stats'])
	
		#--------------------------------------------------------------------------#
		# returncode_write_bed = write_sv_annot_overlap_bed_file(sv_type, annot_type, overlap_sdf, OVRLP_COLS, path_dict["overlap_bed"], shell_script_dir)
		# returncode_write_parquet = write_overlap_sdf_to_disk(sv_type, annot_type, overlap_sdf, OVRLP_COLS, path_dict["overlap_parquet"])
		#--------------------------------------------------------------------------#




def test_tissue_specific_fxns(annot_type, annot_var_dict, path_dict, overlap_sdf, shell_script_dir, spark, num_tiss_cat=32):
	print("Running test_fxns")
	
	######################### variable setup
	full_annot_cols_dict = annot_var_dict[annot_type]['rename_full_annot_dict']
	# rename_ft_dict = dict(full_annot_cols_dict, **annot_var_dict[annot_type]['rename_ft_dict'])
	
	if annot_var_dict[annot_type]['rename_ft_dict']:
		rename_ft_dict = dict(annot_var_dict[annot_type]['rename_full_annot_dict'], **annot_var_dict[annot_type]['rename_ft_dict'])
	else:
		rename_ft_dict = annot_var_dict[annot_type]['rename_full_annot_dict']
	
	if not annot_var_dict[annot_type]['feature_name_col']:
		FT_COLS.remove('ft_name')
	
	full_annot_cols = SV_COLS + ['num_subj'] + FT_COLS[:-1] + OVRLP_COLS + [OVRLP_COLS[0].strip('chr') + 'len',  'sv_len', 'ft_len', 'proportion_of_sv', 'proportion_of_ft']
	
	
	######################### process GIGGLE results --> sv-annot overlap sDF
	overlap_sdf = extract_overlap_data(overlap_sdf, SV_COLS, FT_COLS, OVRLP_COLS)
	print_sdf(overlap_sdf)
	
	if annot_var_dict[annot_type]['tissue_info']:
		tiss_info_file_path = annot_var_dict[annot_type]['tissue_info_file_path']
		
		## add Tissue information to overlap DataFrame
		overlap_sdf = add_tissue_info_from_file(overlap_sdf, tiss_info_file_path, TISSUE_INFO_COLS, FT_COLS[-1], TISSUE_INFO_COLS[0], spark)
		print_sdf(overlap_sdf)
		
		#-------------------------------------------------------------------
		######################### generate TCM
		## TEMP variables --> #TODO: make global variable!!
		# tcm_group_cols = SV_COLS + ['sv_type', 'sv_len'] + OVRLP_COLS
		tcm_group_cols = SV_COLS + OVRLP_COLS
		
		
		list_TCM_sdf = generate_list_TCM(overlap_sdf, SV_COLS, tcm_group_cols, 'tissue_name', 'tiss_cat_name', 'tiss_cat_id', num_tiss_cat)
		# print_sdf(list_TCM_sdf)
		
		tcm_cols_show = SV_COLS + ['overlap_start', 'overlap_end', 'tissue_list', 'tc1', 'tc2', 'tc3', 'tc32']
		# print_sdf(list_TCM_sdf[[tcm_cols_show]])
		list_TCM_sdf[[tcm_cols_show]].show(4)
	



