



####################################################################
#### 1) plot feature summary stats  [ !!! NEED to fully implement !!! ]
# **@TODO: write plot functions!!!**
####################################################################
# 1) DECIDE what program / language to use for plots:
#  - GGPLOT2 in R??
#  - Seaborn?
# 2) DECIDE WHAT should be plotted for each annotation type
#  - annotation feature size distribution
#      - overall
#      - in Step 5:
#         - by individual tissue type
#         - by tissue category
#  - number of overlaps per annotation feature subtype
#      - eg: # overlaps per HOMER TF type
#  - proportion of SVs with overlapping annotation feature
#      - overall
#      - in Step 5:
#         - by individual tissue type
#         - by tissue category
# 3) write plot functions... turn into scripts?
#-----------------------------------------------------------------#

## NEED to fully implement!!!
def extract_and_plot_annotation_summary_stats(overlap_sdf, annot_type, feature_type, plot_script_list, plot_dir):
	
	## 1) set up variables / file names
	plot_file_name_size = annot_type + "_size_distribution"
	plot_file_name_count = annot_type + "_num_overlaps_per_type"
	
	plot_name_size = "Distributions of lengths of " + annot_type + " " + feature_type + "s overlapped by SVs"
	plot_name_count = "Number of overlapping SVs per " + annot_type + " " + feature_type + " type"
	
	
	## 2) extract feature data from overlap sDF
	ft_sdf = overlap_sdf.select(['ft_name', 'ft_chr', 'ft_start', 'ft_end', 'ft_len', 'proportion_of_ft'])
	
	
	## 3) call plot functions / scripts
###################################################################################
## NEED TO IMPLEMENT!!!!
	
	for plot_script in plot_script_list:
		
		## TEMPORARY! replace with actual function / script call
		print("plot_script(ft_sdf, plot_name, file_name, plot_path)")
		
# #         plot_script(ft_sdf, plot_name, file_name, plot_path)
###################################################################################
	
	return ft_sdf  ##change to plot_script return code









####################################################################
#### 2) plot annotation count summary stats by SV
# **TODO** implement SV summary stat plots **
####################################################################
####### >DECIDE:
#  - what language / program to use
#  - plot # overlaps per SV vs SV length
#------------------------------------------------------------------#











##################################################################################################
## ** "driver" fxn that runs the program:
##################################################################################################
#TODO: pass in SV type
def generate_plots(annot_type, annot_var_dict, path_dict, overlap_sdf, shell_script_dir, spark):
# def process_giggle_results(annot_type, annot_var_dict, shell_script_dir, spark):
	feature_type = annot_var_dict[annot_type]['feature_type']
	plot_script_list = ['plot_size_distribution', 'plot_num_per_ft_type']

	test_plot = extract_and_plot_annotation_summary_stats(overlap_sdf, annot_type, feature_type, plot_script_list, path_dict["plots"])
	test_plot.show(3)

